﻿using SSC;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSCSample
{

    /// <summary>
    /// Sample two bezier mesh
    /// </summary>
    [ExecuteInEditMode]
    public class SampleTwoBezierMeshScript : MonoBehaviour
    {

        /// <summary>
        /// Transform points
        /// </summary>
        [Serializable]
        class TransformPoints
        {

            /// <summary>
            /// Point 0
            /// </summary>
            [Tooltip("Point 0")]
            public Transform p0 = null;

            /// <summary>
            /// Point 1
            /// </summary>
            [Tooltip("Point 1")]
            public Transform p1 = null;

            /// <summary>
            /// Point 2
            /// </summary>
            [Tooltip("Point 2")]
            public Transform p2 = null;

            /// <summary>
            /// Point 3
            /// </summary>
            [Tooltip("Point 3")]
            public Transform p3 = null;

            /// <summary>
            /// Start normalized point
            /// </summary>
            [Tooltip("Start normalized point")]
            [Range(0.0f, 1.0f)]
            public float start = 0.0f;

            /// <summary>
            /// End normalized point
            /// </summary>
            [Tooltip("End normalized point")]
            [Range(0.0f, 1.0f)]
            public float end = 1.0f;

            /// <summary>
            /// EquidistantBezierCurve
            /// </summary>
            public EquidistantBezierCurve bezier = new EquidistantBezierCurve();

            /// <summary>
            /// Ara all points available
            /// </summary>
            /// <returns>yes</returns>
            public bool isAllAvaiablePoints()
            {
                return
                    this.p0 &&
                    this.p1 &&
                    this.p2 &&
                    this.p3
                    ;
            }

        }

        /// <summary>
        /// TransformPoints A
        /// </summary>
        [SerializeField]
        [Tooltip("TransformPoints A")]
        TransformPoints m_pointsA = new TransformPoints();

        /// <summary>
        /// TransformPoints B
        /// </summary>
        [SerializeField]
        [Tooltip("TransformPoints B")]
        TransformPoints m_pointsB = new TransformPoints();

        /// <summary>
        /// Sampling unit meter
        /// </summary>
        [SerializeField]
        [Tooltip("Sampling unit meter")]
        [Range(0.1f, 10.0f)]
        float m_samplingUnitMeter = 1.0f;

        [Space(30.0f), Header("Road Mesh")]

        /// <summary>
        /// Reference to MeshFilter for a bezier road mesh
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to MeshFilter for a bezier road mesh")]
        MeshFilter m_refMeshFilter = null;

        ///// <summary>
        ///// Number of cuts U for road
        ///// </summary>
        //[SerializeField]
        //[Tooltip("Number of cuts U for road")]
        //[Range(2, 10)]
        //int m_numberOfCutsU = 2;

        /// <summary>
        /// Repeat meter value for uv V
        /// </summary>
        [SerializeField]
        [Tooltip("Repeat meter value for uv V")]
        [Range(0.1f, 10.0f)]
        float m_repeatUvMeterV = 1.0f;

        ///// <summary>
        ///// Use uv perspective correction
        ///// </summary>
        //[SerializeField]
        //[Tooltip("Use uv perspective correction")]
        //bool m_useUvPerspectiveCorrection = true;

        /// <summary>
        /// Road mesh
        /// </summary>
        [HideInInspector]
        [SerializeField]
        Mesh m_mesh = null;

#if UNITY_EDITOR

        [Space(30.0f), Header("EditoOnly")]

        /// <summary>
        /// Position for m_refAlignTarget (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Position for m_refAlignTarget (EditorOnly)")]
        [Range(0.0f, 1.0f)]
        float m_alignTargetPositionEditorOnly = 0.0f;

        /// <summary>
        /// Draw gizmo lines (EditorOnly)
        /// </summary>
        [Space(30.0f)]
        [SerializeField]
        [Tooltip("Draw gizmo lines (EditorOnly)")]
        bool m_drawGizmoLinesEditorOnly = true;

#endif

        /// <summary>
        /// Awake
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void Awake()
        {

            this.updateBezier();

        }

#if UNITY_EDITOR

        /// <summary>
        /// Update
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void Update()
        {

            if (Application.isPlaying)
            {
                return;
            }

            // ------------------

            //
            {
                this.updateBezier();
                this.updateMesh();
            }

        }

#endif

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void OnDestroy()
        {

            if (Application.isPlaying)
            {
                Destroy(this.m_mesh);
            }

            else
            {
                DestroyImmediate(this.m_mesh);
            }

        }

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void OnDrawGizmos()
        {

#if UNITY_EDITOR

            if (UnityEditor.EditorApplication.isCompiling || !this.m_drawGizmoLinesEditorOnly)
            {
                return;
            }


            if (this.m_pointsA.isAllAvaiablePoints())
            {
                this.m_pointsA.bezier.drawDebugBezierLineEditorOnly();
                this.m_pointsA.bezier.drawDebugBezierPointLinesEditorOnly();
                this.m_pointsA.bezier.drawDebugBezierPositionInfoEditorOnly(this.m_alignTargetPositionEditorOnly);
            }

            if (this.m_pointsB.isAllAvaiablePoints())
            {
                this.m_pointsB.bezier.drawDebugBezierLineEditorOnly();
                this.m_pointsB.bezier.drawDebugBezierPointLinesEditorOnly();
                this.m_pointsB.bezier.drawDebugBezierPositionInfoEditorOnly(this.m_alignTargetPositionEditorOnly);
            }

#endif

        }

        /// <summary>
        /// Update bezier
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void updateBezier()
        {

            if (this.m_pointsA.isAllAvaiablePoints())
            {
                this.m_pointsA.bezier.update(
                    this.m_pointsA.p0.position,
                    this.m_pointsA.p1.position,
                    this.m_pointsA.p2.position,
                    this.m_pointsA.p3.position,
                    this.m_samplingUnitMeter,
                    this.m_pointsA.p0.up,
                    this.m_pointsA.p3.up
                    );
            }

            if (this.m_pointsB.isAllAvaiablePoints())
            {
                this.m_pointsB.bezier.update(
                    this.m_pointsB.p0.position,
                    this.m_pointsB.p1.position,
                    this.m_pointsB.p2.position,
                    this.m_pointsB.p3.position,
                    this.m_samplingUnitMeter,
                    this.m_pointsB.p0.up,
                    this.m_pointsB.p3.up
                    );
            }

        }

        /// <summary>
        /// Update mesh
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void updateMesh()
        {

            if (this.m_refMeshFilter)
            {

                if (!this.m_mesh)
                {
                    this.m_mesh = new Mesh();
                    this.m_mesh.name = this.name;
                }

                this.m_refMeshFilter.sharedMesh = this.m_mesh;

                this.m_pointsA.bezier.createSimpleRoadWithOtherBezierVer2(
                    this.m_pointsB.bezier,
                    this.m_pointsA.start,
                    this.m_pointsA.end,
                    this.m_pointsB.start,
                    this.m_pointsB.end,
                    this.m_repeatUvMeterV,
                    this.m_mesh,
                    true,
                    Vector3.up,
                    this.m_refMeshFilter.transform
                    );

            }

            else
            {

                if (this.m_mesh)
                {
                    this.OnDestroy();
                }

            }

        }

    }

}
