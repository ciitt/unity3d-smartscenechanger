﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSCSample
{

    public class SampleScrollViewUiGroup : RecycleScrollViewUiGroup<SampleScrollViewUiContent>
    {

        [SerializeField]
        Vector2 m_contentUiSize = new Vector2(200, 100);

        void Start()
        {

            for (int i = 0; i < 100; i++)
            {
                this.addContent(new SampleScrollViewUiContent(i.ToString(), this.m_contentUiSize), false);
            }

        }

    }

}
