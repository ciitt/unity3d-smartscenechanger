﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSCSample
{

    public class SampleScrollViewUiContent : RecycleScrollViewUiContent
    {

        public string text = "";

        public SampleScrollViewUiContent(
            string _text,
            Vector2 _uiSizeDelta
            ) : base(_uiSizeDelta)
        {

            this.text = _text;

        }

    }

}
