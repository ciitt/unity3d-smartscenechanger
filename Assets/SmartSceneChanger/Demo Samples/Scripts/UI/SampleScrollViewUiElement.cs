﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SSCSample
{

    public class SampleScrollViewUiElement : RecycleScrollViewUiElement<SampleScrollViewUiContent>
    {

        [SerializeField]
        Text m_refText = null;

        public override void setContent(SampleScrollViewUiContent contentOrNull)
        {
            
            if (this.m_refText)
            {
                this.m_refText.text = (contentOrNull != null) ? contentOrNull.text : "";
            }

        }

        public void OnClick()
        {

            if (this.m_refCurrentContent != null)
            {
                print(this.m_refCurrentContent.text);
            }

        }

    }

}
