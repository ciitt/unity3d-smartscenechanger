﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Minimum number attribute
    /// </summary>
    public class MinNumberAttribute : PropertyAttribute
    {

        public double min = 0;

        public MinNumberAttribute(double _min)
        {
            this.min = _min;
        }

    }

}
