﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Minimum number PropertyDrawer
    /// </summary>
    [CustomPropertyDrawer(typeof(MinNumberAttribute))]
    public class MinNumberDrawer : PropertyDrawer
    {

        /// <summary>
        /// OnGUI
        /// </summary>
        /// <param name="position">Rect</param>
        /// <param name="property">SerializedProperty</param>
        /// <param name="label">GUIContent</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {

            EditorGUI.BeginChangeCheck();

            EditorGUI.PropertyField(position, property, label);

            if (EditorGUI.EndChangeCheck())
            {

                MinNumberAttribute mna = (MinNumberAttribute)this.attribute;

                if (property.propertyType == SerializedPropertyType.Integer)
                {
                    property.longValue = Math.Max((long)mna.min, property.longValue);
                }

                else if (property.propertyType == SerializedPropertyType.Float)
                {
                    property.doubleValue = Math.Max((double)mna.min, property.doubleValue);
                }


            }

        }

    }

}
