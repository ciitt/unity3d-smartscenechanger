﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC
{

    /// <summary>
    /// Simple spawnable object
    /// </summary>
    public abstract class SimpleSpawnObject<T> : SpawnObject<T> where T : SimpleSpawnData
    {

        /// <summary>
        /// Reference to target
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to target")]
        protected Transform m_refTarget = null;

        // -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to camera to check if to spawn or not
        /// </summary>
        /// <returns>camera</returns>
        protected abstract Camera refCamera();

        /// <summary>
        /// Should despawn
        /// </summary>
        /// <returns>yes</returns>
        // -------------------------------------------------------------------------------------------------
        public override bool shouldDespawn()
        {

            Camera camera = this.refCamera();

            if (camera)
            {
                return !Funcs.IsPositionVisibleByCamera(camera, this.m_refTarget.position, 1.2f, 5.0f);
            }

            return true;

        }

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected override void Start()
        {

            // base
            {
                base.Start();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refTarget)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_refTarget == null : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
