﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC
{

    /// <summary>
    /// Spawn data
    /// </summary>
    public abstract class SimpleSpawnData : SpawnData
    {

        /// <summary>
        /// World position to spawn
        /// </summary>
        /// <returns>position</returns>
        protected abstract Vector3 worldPositionToSpawn();

        /// <summary>
        /// Reference to camera to check if to spawn or not
        /// </summary>
        /// <returns>camera</returns>
        protected abstract Camera refCamera();

        /// <summary>
        /// Should spawn
        /// </summary>
        /// <returns>yes</returns>
        // ------------------------------------------------------------------------------------------------
        public override bool shouldSpawn()
        {

            Camera camera = this.refCamera();

            if (camera)
            {
                return Funcs.IsPositionVisibleByCamera(camera, this.worldPositionToSpawn(), 1.0f, 3.0f);
            }

            return false;
            
        }

    }

}
