﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC
{

    /// <summary>
    /// Spawn group
    /// </summary>
    /// <typeparam name="T1">SpawnData</typeparam>
    /// <typeparam name="T2">SpawnObject</typeparam>
    public abstract class SpawnGroup<T1, T2> : MonoBehaviour where T1 : SpawnData where T2 : SpawnObject<T1>
    {

        /// <summary>
        /// Mac iterate count per frame. (if 0, iterate all)
        /// </summary>
        [SerializeField]
        [Tooltip("Mac iterate count per frame. (if 0, iterate all)")]
        [MinNumber(0)]
        protected int m_maxIterateCountPerFrame = 0;

        /// <summary>
        /// Current data list
        /// </summary>
        protected List<T1> m_currentDataList = new List<T1>();

        /// <summary>
        /// Child spawn object list
        /// </summary>
        protected T2[] m_childSpawnObjectList = new T2[0];

        /// <summary>
        /// Current data index for iterator
        /// </summary>
        protected int m_currentDataIndexForIterator = 0;

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_childSpawnObjectList
            {
                this.m_childSpawnObjectList = this.GetComponentsInChildren<T2>(true);
            }

#if UNITY_EDITOR

            //
            {

                if (this.m_childSpawnObjectList.Length <= 0)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_childSpawnObjectList.Length <= 0 : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // updateAllSpawnObjects
            {
                this.updateAllSpawnObjects();
            }

        }

        /// <summary>
        /// Set data
        /// </summary>
        /// <param name="dataList">data list</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void setDataList(IList<T1> dataList)
        {
            this.m_currentDataIndexForIterator = 0;
            this.clearData();
            this.m_currentDataList.AddRange(dataList);
        }

        /// <summary>
        /// Clear data
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual void clearData()
        {

            this.m_currentDataIndexForIterator = 0;

            this.m_currentDataList.Clear();

            foreach (var val in this.m_childSpawnObjectList)
            {

                if (val)
                {
                    val.updateObject(null);
                }

            }

        }

        /// <summary>
        /// Add data
        /// </summary>
        /// <param name="data">data</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void addData(T1 data)
        {
            this.m_currentDataIndexForIterator = 0;
            this.m_currentDataList.Add(data);
        }

        /// <summary>
        /// Remove data
        /// </summary>
        /// <param name="data">data</param>
        // -----------------------------------------------------------------------------------------------
        public virtual bool removeData(T1 data)
        {

            this.m_currentDataIndexForIterator = 0;

            foreach (var val in this.m_childSpawnObjectList)
            {

                if (val && val.refCurrentData == data)
                {
                    val.updateObject(null);
                }

            }

            return this.m_currentDataList.Remove(data);

        }

        /// <summary>
        /// Function when detected too many data to show
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void detectedTooManyDataToShow()
        {

#if UNITY_EDITOR
            Debug.LogWarningFormat("Too many data to show, so you should add more SpawnObjects : {0}", Funcs.CreateHierarchyPath(this.transform));
#endif

        }

        /// <summary>
        /// Update all spawn objects
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void updateAllSpawnObjects()
        {

            T1 data = null;

            T2 spawnObject = null;

            int objectIndex = 0;
            int dataIndex = 0;

            int allDataCount = this.m_currentDataList.Count;

            int checkCount = 0;

            // ----------------------

            if (allDataCount <= 0)
            {
                return;
            }

            // ----------------------

            // checkCount, m_currentDataIndexForIterator
            {

                if (this.m_maxIterateCountPerFrame > 0)
                {
                    checkCount = this.m_maxIterateCountPerFrame;
                    this.m_currentDataIndexForIterator = Mathf.Max(0, this.m_currentDataIndexForIterator);
                }

                else
                {
                    checkCount = allDataCount;
                    this.m_currentDataIndexForIterator = 0;
                }

            }

            // updateObject
            {

                for (int i = 0; i < checkCount; i++)
                {

                    dataIndex = this.m_currentDataIndexForIterator;

                    if (objectIndex >= this.m_childSpawnObjectList.Length)
                    {
                        this.detectedTooManyDataToShow();
                        break;
                    }

                    // -------------------------

                    data = this.m_currentDataList[dataIndex];

                    // -------------------------

                    if (data == null)
                    {
                        continue;
                    }

                    // -------------------------

                    if (data.shouldSpawn() && !data.refSpawnObject)
                    {

                        for (; objectIndex < this.m_childSpawnObjectList.Length; objectIndex++)
                        {

                            spawnObject = this.m_childSpawnObjectList[objectIndex];

                            if (spawnObject)
                            {

                                if (
                                    spawnObject.refCurrentData == null ||
                                    spawnObject.shouldDespawn()
                                    )
                                {
                                    spawnObject.updateObject(data);
                                    objectIndex++;
                                    break;
                                }

                            }

                        }

                    }

                    // increment
                    {
                        this.m_currentDataIndexForIterator = (this.m_currentDataIndexForIterator + 1) % allDataCount;
                    }

                }

            }

            // set null
            {

                for (; objectIndex < this.m_childSpawnObjectList.Length; objectIndex++)
                {

                    spawnObject = this.m_childSpawnObjectList[objectIndex];

                    if (spawnObject)
                    {

                        if (
                            spawnObject.refCurrentData != null &&
                            spawnObject.shouldDespawn()
                            )
                        {
                            spawnObject.updateObject(null);
                        }

                    }

                }

            }

        }

    }

}
