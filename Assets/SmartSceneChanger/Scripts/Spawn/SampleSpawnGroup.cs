﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSCSample
{

    /// <summary>
    /// Sample spawn group
    /// </summary>
    public class SampleSpawnGroup : SimpleSpawnGroup<SampleSpawnData, SampleSpawnObject>
    {

        /// <summary>
        /// Test data count
        /// </summary>
        [SerializeField]
        [Tooltip("Test data count")]
        int m_testDataCount = 100;

        /// <summary>
        /// Start
        /// </summary>
        // ----------------------------------------------------------------
        protected override void Start()
        {

            // base
            {
                base.Start();
            }

            // addData
            {

                float randPos = 50.0f;

                Vector3 pos = Vector3.zero;
                Quaternion rot = Quaternion.identity;

                for (int i = 0; i < this.m_testDataCount; i++)
                {

                    pos = new Vector3(
                        UnityEngine.Random.Range(-randPos, randPos),
                        UnityEngine.Random.Range(-randPos, randPos),
                        UnityEngine.Random.Range(-randPos, randPos)
                        );

                    rot = Quaternion.Euler(
                        UnityEngine.Random.Range(-180f, 180f),
                        UnityEngine.Random.Range(-180f, 180f),
                        UnityEngine.Random.Range(-180f, 180f)
                        );

                    this.addData(new SampleSpawnData(pos, rot, this));

                }

            }

        }

    }

}
