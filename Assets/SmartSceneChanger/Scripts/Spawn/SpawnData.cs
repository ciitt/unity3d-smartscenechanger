﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC
{

    /// <summary>
    /// Spawn data
    /// </summary>
    public abstract class SpawnData
    {

        /// <summary>
        /// Reference to SpawnObject
        /// </summary>
        public MonoBehaviour refSpawnObject = null;

        // ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Should spawn
        /// </summary>
        /// <returns>yes</returns>
        public abstract bool shouldSpawn();

    }

}
