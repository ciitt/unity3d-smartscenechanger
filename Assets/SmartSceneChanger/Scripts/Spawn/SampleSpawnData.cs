﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSCSample
{

    /// <summary>
    /// Sample spawn data
    /// </summary>
    public class SampleSpawnData : SimpleSpawnData
    {

        public Vector3 spawnPosition = Vector3.zero;
        public Quaternion spawnRotation = Quaternion.identity;

        SampleSpawnGroup refSampleSpawnGroup = null;

        /// <summary>
        /// Reference to camera
        /// </summary>
        /// <returns>camera</returns>
        // ------------------------------------------------------------------------------------------------
        protected override Camera refCamera()
        {
            return
                (this.refSampleSpawnGroup) ?
                this.refSampleSpawnGroup.refCamera :
                null
                ;
        }

        /// <summary>
        /// World position to spawn
        /// </summary>
        /// <returns>position</returns>
        // ------------------------------------------------------------------------------------------------
        protected override Vector3 worldPositionToSpawn()
        {
            return this.spawnPosition;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_spawnPosition">spawnPosition</param>
        /// <param name="_spawnRotation">spawnRotation</param>
        /// <param name="_refSampleSpawnGroup">refSampleSpawnGroup</param>
        // ------------------------------------------------------------------------------------------------
        public SampleSpawnData(
            Vector3 _spawnPosition,
            Quaternion _spawnRotation,
            SampleSpawnGroup _refSampleSpawnGroup
            ) : base()
        {
            this.spawnPosition = _spawnPosition;
            this.spawnRotation = _spawnRotation;
            this.refSampleSpawnGroup = _refSampleSpawnGroup;
        }

    }

}
