﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SSC
{

    /// <summary>
    /// Spawnable object
    /// </summary>
    /// <typeparam name="T">SpawnData</typeparam>
    public abstract class SpawnObject<T> : MonoBehaviour where T : SpawnData
    {

        /// <summary>
        /// Reference to current data
        /// </summary>
        protected T m_refCurrentData = null;

        /// <summary>
        /// Set data
        /// </summary>
        /// <typeparam name="T">SpawnData</typeparam>
        /// <param name="dataOrNull">data or null</param>
        public abstract void setData(T dataOrNull);

        /// <summary>
        /// Should despawn
        /// </summary>
        /// <returns>yes</returns>
        public abstract bool shouldDespawn();

        // -----------------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to current data
        /// </summary>
        public T refCurrentData
        {
            get { return this.m_refCurrentData; }
        }

        /// <summary>
        /// Start
        /// </summary>
        // -----------------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // hide
            {
                this.ShowOrHideIfNeeded(false);
            }

        }

        /// <summary>
        /// Update object
        /// </summary>
        /// <param name="dataOrNull">data or null</param>
        // -----------------------------------------------------------------------------------------------------
        public virtual void updateObject(T dataOrNull)
        {

            // refSpawnObject
            {

                if (this.m_refCurrentData != null)
                {
                    this.m_refCurrentData.refSpawnObject = null;
                }

                if (dataOrNull != null)
                {
                    dataOrNull.refSpawnObject = this;
                }

            }

            // m_refCurrentData
            {
                this.m_refCurrentData = dataOrNull;
            }

            // setData
            {
                this.setData(dataOrNull);
            }

            // ShowOrHideIfNeeded
            {
                this.ShowOrHideIfNeeded(dataOrNull != null);
            }

        }

        /// <summary>
        /// Show or hide if needed
        /// </summary>
        /// <param name="show">true to show, false to hide</param>
        // -----------------------------------------------------------------------------------------------------
        protected virtual void ShowOrHideIfNeeded(bool show)
        {

            if (show && !this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(true);
            }

            else if (!show && this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(false);
            }

        }

    }

}
