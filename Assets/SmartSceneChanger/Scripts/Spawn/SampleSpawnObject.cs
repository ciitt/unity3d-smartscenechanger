﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSCSample
{

    /// <summary>
    /// Sample spawn object
    /// </summary>
    public class SampleSpawnObject : SimpleSpawnObject<SampleSpawnData>
    {

        /// <summary>
        /// Reference to SampleSpawnGroup
        /// </summary>
        SampleSpawnGroup m_refSpawnGroup = null;

        /// <summary>
        /// Rotate speed
        /// </summary>
        float m_rotateSpeed = 0.0f;

        /// <summary>
        /// Set data
        /// </summary>
        /// <param name="dataOrNull">data or null</param>
        // ------------------------------------------------------------------------------------------------
        public override void setData(SampleSpawnData dataOrNull)
        {

            if (
                dataOrNull != null &&
                this.m_refTarget
                )
            {
                this.m_refTarget.position = dataOrNull.spawnPosition;
                this.m_refTarget.rotation = dataOrNull.spawnRotation;
            }

        }

        /// <summary>
        /// Reference to camera
        /// </summary>
        /// <returns>camera</returns>
        // ------------------------------------------------------------------------------------------------
        protected override Camera refCamera()
        {
            return
                (this.m_refSpawnGroup) ?
                this.m_refSpawnGroup.refCamera :
                null
                ;
        }

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected override void Start()
        {

            // base
            {
                base.Start();
            }

            // m_refSpawnGroup
            {
                this.m_refSpawnGroup = this.GetComponentInParent<SampleSpawnGroup>();
            }

            // m_rotateSpeed
            {
                this.m_rotateSpeed = UnityEngine.Random.Range(-1.0f, 1.0f);
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refSpawnGroup)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_refSpawnGroup == null : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif
        }

        /// <summary>
        /// Update
        /// </summary>
        // ------------------------------------------------------------------------------------------
        private void Update()
        {
            
            if (this.m_refTarget && this.m_refCurrentData != null)
            {
                this.m_refTarget.Rotate(this.m_rotateSpeed, this.m_rotateSpeed, this.m_rotateSpeed);
            }

        }

    }

}