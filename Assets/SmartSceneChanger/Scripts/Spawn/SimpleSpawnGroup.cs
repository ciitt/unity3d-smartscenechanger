﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Simple spawn group
    /// </summary>
    //public class SimpleSpawnGroup : SpawnGroup<SimpleSpawnData, SimpleSpawnObject>
    public abstract class SimpleSpawnGroup<T1, T2> : SpawnGroup<T1, T2> where T1 : SpawnData where T2 : SpawnObject<T1>
    {

        /// <summary>
        /// Reference to camera
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to camera")]
        protected Camera m_refCamera = null;

        // -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to camera
        /// </summary>
        public Camera refCamera
        {
            get { return this.m_refCamera; }
        }

        /// <summary>
        /// Start
        /// </summary>
        // -------------------------------------------------------------------------------------------------
        protected override void Start()
        {

            // base
            {
                base.Start();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCamera)
                {
                    Debug.LogWarningFormat("(#if UNITY_EDITOR) : m_refCamera == null : {0}", Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

    }

}
