﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Equidistant point list
    /// </summary>
    [Serializable]
    public class EquidistantPointList
    {

        /// <summary>
        /// Sampling unit meter
        /// </summary>
        public float samplingUnitMeter = 0.0f;

        /// <summary>
        /// Sampled point list
        /// </summary>
        public List<Vector3> sampledPoints = new List<Vector3>();

        /// <summary>
        /// Sampled total meter length
        /// </summary>
        public float sampledTotalMeterLength = 0.0f;

        /// <summary>
        /// Length list for sampling
        /// </summary>
        public List<float> sampledLengthList = new List<float>();

        /// <summary>
        /// Original point list
        /// </summary>
        static List<Vector3> OriginalPoints = new List<Vector3>();

        /// <summary>
        /// Original total meter length
        /// </summary>
        static float OriginalTotalMeterLength = 0.0f;

        /// <summary>
        /// Original point length list
        /// </summary>
        static List<float> OriginalLengthList = new List<float>();

        // ---------------------------------------------------------------------------------------

        /// <summary>
        /// Sampled point count
        /// </summary>
        public int sampledPointCount { get { return this.sampledPoints.Count; } }

        /// <summary>
        /// Constructor
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public EquidistantPointList()
        {

        }

        /// <summary>
        /// Is valid parameters
        /// </summary>
        /// <returns></returns>
        // ---------------------------------------------------------------------------------------
        protected virtual bool isValidParameters()
        {
            return (
                this.sampledPointCount > 1 &&
                this.sampledLengthList.Count > 0 &&
                this.sampledLengthList.Count + 1 == this.sampledPoints.Count
                );
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_points">point list</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public EquidistantPointList(
            IList<Vector3> _points,
            float _samplingUnitMeter
            )
        {

            // init
            {
                this.update(_points, _samplingUnitMeter);
            }

        }

        /// <summary>
        /// Find a sampled point by normalized value
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>point</returns>
        // ---------------------------------------------------------------------------------------
        public virtual Vector3 findSampledPointByNormalizeValue(float t01)
        {
            return this.findSampledPointByMeter(this.sampledTotalMeterLength * t01);
        }

        /// <summary>
        /// Find a sampled point by meter value
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <returns>point</returns>
        // ---------------------------------------------------------------------------------------
        public virtual Vector3 findSampledPointByMeter(float targetMeter)
        {

            if (!this.isValidParameters())
            {
                return Vector3.zero;
            }

            // -----------------------------

            float t01 = (this.sampledTotalMeterLength > 0.0f) ? Mathf.Clamp01(targetMeter / this.sampledTotalMeterLength) : 0.0f;

            int size = this.sampledLengthList.Count;

            float point_t01 = 0.0f;

            int i = Mathf.RoundToInt(t01 * (size - 1));

            float baseLength = this.sampledLengthList[i];

            // -----------------------------

            if (targetMeter <= this.sampledLengthList[0])
            {

                point_t01 = Mathf.InverseLerp(
                    0.0f,
                    this.sampledLengthList[0],
                    targetMeter
                    );

                return Vector3.Lerp(this.sampledPoints[0], this.sampledPoints[1], point_t01);

            }

            if (targetMeter > this.sampledTotalMeterLength)
            {
                return this.sampledPoints[this.sampledPoints.Count - 1];
            }

            // -----------------------------

            if (targetMeter <= baseLength)
            {

                float length1 = 0.0f;
                float length2 = 0.0f;

                for (; i >= 1; i--)
                {

                    length1 = this.sampledLengthList[i - 1];
                    length2 = this.sampledLengthList[i];

                    if (length1 <= targetMeter && targetMeter <= length2)
                    {

                        point_t01 = Mathf.InverseLerp(
                            length1,
                            length2,
                            targetMeter
                            );

                        return Vector3.Lerp(this.sampledPoints[i], this.sampledPoints[i + 1], point_t01);

                    }

                }

            }

            else
            {

                i = Mathf.Max(1, i);

                float length1 = 0.0f;
                float length2 = 0.0f;

                for (; i < size; i++)
                {

                    length1 = this.sampledLengthList[i - 1];
                    length2 = this.sampledLengthList[i];

                    if (targetMeter <= this.sampledLengthList[i])
                    {

                        point_t01 = Mathf.InverseLerp(
                            this.sampledLengthList[i - 1],
                            this.sampledLengthList[i],
                            targetMeter
                            );

                        return Vector3.Lerp(this.sampledPoints[i], this.sampledPoints[i + 1], point_t01);

                    }

                }

            }

            return Vector3.zero;

        }

        /// <summary>
        /// Update values
        /// </summary>
        /// <param name="_points">point list</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public virtual void update(
            IList<Vector3> _points,
            float _samplingUnitMeter
            )
        {
            
            // clear
            {
                OriginalPoints.Clear();
                OriginalLengthList.Clear();
                OriginalTotalMeterLength = 0.0f;
            }

            // samplingUnitMeter
            {
                this.samplingUnitMeter = _samplingUnitMeter;
            }

            // OriginalPoints
            {
                OriginalPoints.AddRange(_points);
            }

            // totalMeterLength, OriginalLengthList
            {

                int size = OriginalPoints.Count;

                for (int i = 0; i < size - 1; i++)
                {
                    OriginalTotalMeterLength += Vector3.Distance(OriginalPoints[i], OriginalPoints[i + 1]);
                    OriginalLengthList.Add(OriginalTotalMeterLength);
                }

            }

            // sample
            {
                this.sample();
            }

            // clear
            {
                OriginalPoints.Clear();
                OriginalLengthList.Clear();
                OriginalTotalMeterLength = 0.0f;
            }

        }

        /// <summary>
        /// Sampling
        /// </summary>
        // ---------------------------------------------------------------------------------------
        protected virtual void sample()
        {

            if (
                OriginalLengthList.Count + 1 != OriginalPoints.Count
                )
            {
                return;
            }

            // -----------------------

            this.sampledPoints.Clear();
            this.sampledLengthList.Clear();
            this.sampledTotalMeterLength = 0.0f;

            // -----------------------

            int sampling =
                (samplingUnitMeter > 0.0f) ?
                Mathf.Max(2, Mathf.RoundToInt(OriginalTotalMeterLength / this.samplingUnitMeter)) :
                2
                ;

            float targetMeter = 0.0f;

            float sampling_t01 = 0.0f;
            float point_t01 = 0.0f;

            int j = 0;

            int size = OriginalLengthList.Count;

            // -----------------------

            // sampledPoints
            {

                for (int i = 0; i < sampling; i++)
                {

                    sampling_t01 = i / (float)(sampling - 1);

                    targetMeter = OriginalTotalMeterLength * sampling_t01;

                    for (; j < size; j++)
                    {

                        if (targetMeter <= OriginalLengthList[j])
                        {

                            point_t01 = Mathf.InverseLerp(
                                (j > 0) ? OriginalLengthList[j - 1] : 0.0f,
                                OriginalLengthList[j],
                                targetMeter
                                );

                            this.sampledPoints.Add(Vector3.Lerp(OriginalPoints[j], OriginalPoints[j + 1], point_t01));

                            break;

                        }

                    }

                }

            }

            // sampledLengthList
            {

                size = this.sampledPoints.Count;

                for (int i = 0; i < size - 1; i++)
                {
                    this.sampledTotalMeterLength += Vector3.Distance(this.sampledPoints[i], this.sampledPoints[i + 1]);
                    this.sampledLengthList.Add(this.sampledTotalMeterLength);
                }

            }

        }

        /// <summary>
        /// Find sampled points
        /// </summary>
        /// <param name="dest">return</param>
        /// <param name="numberOfCuts">number of cuts (val > 1)</param>
        // ---------------------------------------------------------------------------------------
        public void findSampledPoints(List<Vector3> dest, int numberOfCuts)
        {

            if (numberOfCuts <= 1)
            {
                return;
            }

            // -------------------

            dest.Clear();

            float lerpVal = 0.0f;

            // -------------------

            for (int i = 0; i < numberOfCuts; i++)
            {

                lerpVal = i / (float)(numberOfCuts - 1);

                dest.Add(this.findSampledPointByNormalizeValue(lerpVal));

            }

        }

        /// <summary>
        /// Find a closest position from pointP
        /// </summary>
        /// <param name="pointP">pointP</param>
        /// <param name="position">position</param>
        /// <param name="from">from</param>
        /// <param name="to">to</param>
        // ---------------------------------------------------------------------------------------
        public void findClosestPositionOnLines(
            Vector3 pointP,
            out Vector3 position,
            out Vector3 from,
            out Vector3 to
            )
        {

            position = Vector3.zero;
            from = Vector3.zero;
            to = Vector3.zero;

            // --------------

            Vector3 pointA = Vector3.zero;
            Vector3 pointB = Vector3.zero;

            Vector3 vecAtoB = Vector3.zero;
            Vector3 vecAtoP = Vector3.zero;

            float dot = 0.0f;
            float currentMinSqrDistance = float.MaxValue;

            Vector3 tempPosition = Vector3.zero;
            float tempSqrDistance = 0.0f;

            // --------------

            for (int i = 0; i < this.sampledPointCount - 1; i++)
            {

                pointA = this.sampledPoints[i];
                pointB = this.sampledPoints[i + 1];

                vecAtoB = pointB - pointA;
                vecAtoP = pointP - pointA;

                dot = Mathf.Clamp01(Vector3.Dot(vecAtoB, vecAtoP) / vecAtoB.sqrMagnitude);

                tempPosition = Vector3.Lerp(pointA, pointB, dot);

                tempSqrDistance = (pointP - tempPosition).sqrMagnitude;

                if (currentMinSqrDistance > tempSqrDistance)
                {
                    currentMinSqrDistance = tempSqrDistance;
                    position = tempPosition;
                    from = pointA;
                    to = pointB;
                }

            }

        }

    }

}
