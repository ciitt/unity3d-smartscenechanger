﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Equidistant bezier curve
    /// </summary>
    public partial class EquidistantBezierCurve : EquidistantPointList
    {

        /// <summary>
        /// Create simple road with other bezier (Bilinear Interpolation)
        /// </summary>
        /// <param name="otherBezier">other bezier</param>
        /// <param name="thisStartMeterFrom">this start point meter</param>
        /// <param name="thisEndMeterTo">this end point meter</param>
        /// <param name="otherStartMeterFrom">other start point meter</param>
        /// <param name="otherEndMeterTo">other end point meter</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="generateLightmapUv">true to generate lightmap uv (uv2.xy)</param>
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoadWithOtherBezierVer2(
            EquidistantBezierCurve otherBezier,
            float thisStartMeterFrom,
            float thisEndMeterTo,
            float otherStartMeterFrom,
            float otherEndMeterTo,
            float repeatUvMeterV,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform,
            int numberOfCutsV = -1,
            bool generateLightmapUv = false
            )
        {

            // check
            {
                if (
                    otherBezier == null ||
                    !dest
                    )
                {
                    return;
                }
            }

            // -----------------------

            thisStartMeterFrom = Mathf.Max(0, thisStartMeterFrom);
            otherStartMeterFrom = Mathf.Max(0, otherStartMeterFrom);

            thisEndMeterTo = Mathf.Min(thisEndMeterTo, this.sampledTotalMeterLength);
            otherEndMeterTo = Mathf.Min(otherEndMeterTo, otherBezier.sampledTotalMeterLength);

            numberOfCutsV = Mathf.Max(2, (numberOfCutsV < 2) ? this.sampledPointCount : numberOfCutsV);

            int vertexCount = (numberOfCutsV - 1) * 4;

            Vector3[] vertices = new Vector3[vertexCount];
            List<Vector4> uvs0 = new List<Vector4>(new Vector4[vertexCount]); // pos00, pos11.x
            List<Vector4> uvs1 = new List<Vector4>(new Vector4[vertexCount]); // lightmapUv, width00to10, width01to11
            List<Vector4> uvs2 = new List<Vector4>(new Vector4[vertexCount]); // pos10, pos11.y
            List<Vector4> uvs3 = new List<Vector4>(new Vector4[vertexCount]); // pos01, pos11.z
            Vector3[] normals = new Vector3[vertexCount];
            Color32[] colors32 = new Color32[vertexCount]; // currLerpValV, nextLerpValV, unused, 1 / uvScaleY
            int[] triangles = new int[(numberOfCutsV - 1) * 6];

            float thisBezierLength = Mathf.Abs(thisEndMeterTo - thisStartMeterFrom);
            float otherBezierLength = Mathf.Abs(otherEndMeterTo - otherStartMeterFrom);

            float averageBezierLength = (thisBezierLength + otherBezierLength) * 0.5f;

            // ------------------------

            // vertices, uvs (straight along z)
            {

                int trianglesCounter = 0;

                float currLerpValV = 0.0f;
                float nextLerpValV = 0.0f;

                float endUvY = Mathf.Max(1, Mathf.RoundToInt(averageBezierLength / repeatUvMeterV));
                
                Color32 vertexColor = new Color32(0, 0, 0, 0);

                Vector3 pos00 = Vector3.zero;
                Vector3 pos10 = Vector3.zero;
                Vector3 pos01 = Vector3.zero;
                Vector3 pos11 = Vector3.zero;

                Vector3 normal00 = Vector3.zero;
                Vector3 normal10 = Vector3.zero;
                Vector3 normal01 = Vector3.zero;
                Vector3 normal11 = Vector3.zero;

                float meter01 = 0.0f;
                float meter11 = 0.0f;

                float width00to10 = 0.0f;
                float width01to11 = 0.0f;

                bool posFlip = (thisStartMeterFrom > thisEndMeterTo);

                // first
                {

                    float thisStartMeterFromAs01 = this.convertSampledMeterToNormalized01(thisStartMeterFrom);
                    float otherStartMeterFromAs01 = otherBezier.convertSampledMeterToNormalized01(otherStartMeterFrom);

                    pos00 = (useSampledPoints) ? this.findSampledPointByMeter(thisStartMeterFrom) : this.findRawBezierPoint(thisStartMeterFromAs01);
                    pos10 = (useSampledPoints) ? otherBezier.findSampledPointByMeter(otherStartMeterFrom) : otherBezier.findRawBezierPoint(otherStartMeterFromAs01);

                    normal00 = Vector3.Lerp(this.startUpwards, this.endUpwards, thisStartMeterFromAs01).normalized;
                    normal10 = Vector3.Lerp(otherBezier.startUpwards, otherBezier.endUpwards, otherStartMeterFromAs01).normalized;

                    width00to10 = Vector3.Distance(pos00, pos10);

                    if (baseTransform)
                    {
                        pos00 = baseTransform.InverseTransformPoint(pos00);
                        pos10 = baseTransform.InverseTransformPoint(pos10);
                    }

                }

                // vertices, uvs, normals
                {

                    int i = 0;

                    float normalizedMeter01 = 0.0f;
                    float normalizedMeter11 = 0.0f;

                    for (int v = 0, index = 0; v < numberOfCutsV - 1; v++, index += 4)
                    {

                        currLerpValV = v / (float)(numberOfCutsV - 1);
                        nextLerpValV = (v + 1) / (float)(numberOfCutsV - 1);

                        meter01 = Mathf.Lerp(thisStartMeterFrom, thisEndMeterTo, nextLerpValV);
                        meter11 = Mathf.Lerp(otherStartMeterFrom, otherEndMeterTo, nextLerpValV);

                        normalizedMeter01 = this.convertSampledMeterToNormalized01(meter01);
                        normalizedMeter11 = otherBezier.convertSampledMeterToNormalized01(meter11);

                        pos01 = (useSampledPoints) ? this.findSampledPointByMeter(meter01) : this.findRawBezierPoint(normalizedMeter01);
                        pos11 = (useSampledPoints) ? otherBezier.findSampledPointByMeter(meter11) : otherBezier.findRawBezierPoint(normalizedMeter11);

                        normal01 = Vector3.Lerp(this.startUpwards, this.endUpwards, normalizedMeter01).normalized;
                        normal11 = Vector3.Lerp(otherBezier.startUpwards, otherBezier.endUpwards, normalizedMeter11).normalized;

                        width01to11 = Vector3.Distance(pos01, pos11);

                        if (baseTransform)
                        {
                            pos01 = baseTransform.InverseTransformPoint(pos01);
                            pos11 = baseTransform.InverseTransformPoint(pos11);
                        }

                        // set
                        {

                            vertices[index + 0] = pos00;
                            vertices[index + 1] = pos10;
                            vertices[index + 2] = pos01;
                            vertices[index + 3] = pos11;

                            if (!posFlip)
                            {

                                vertexColor.r = (byte)(currLerpValV * 255);
                                vertexColor.g = (byte)(nextLerpValV * 255);
                                vertexColor.b = 0;
                                vertexColor.a = (byte)((1.0f / endUvY) * 255);

                                for (i = 0; i < 4; i++)
                                {
                                    uvs0[index + i] = new Vector4(pos00.x, pos00.y, pos00.z, pos11.x);
                                    uvs1[index + i] = new Vector4(0, 0, width00to10, width01to11);
                                    uvs2[index + i] = new Vector4(pos10.x, pos10.y, pos10.z, pos11.y);
                                    uvs3[index + i] = new Vector4(pos01.x, pos01.y, pos01.z, pos11.z);
                                }

                            }

                            else
                            {

                                vertexColor.g = (byte)(currLerpValV * 255);
                                vertexColor.r = (byte)(nextLerpValV * 255);
                                vertexColor.b = 0;
                                vertexColor.a = (byte)((1.0f / endUvY) * 255);

                                for (i = 0; i < 4; i++)
                                {
                                    uvs0[index + i] = new Vector4(pos01.x, pos01.y, pos01.z, pos10.x);
                                    uvs1[index + i] = new Vector4(0, 0, width01to11, width00to10);
                                    uvs2[index + i] = new Vector4(pos11.x, pos11.y, pos11.z, pos10.y);
                                    uvs3[index + i] = new Vector4(pos00.x, pos00.y, pos00.z, pos10.z);
                                }

                            }

                            normals[index + 0] = normal00;
                            normals[index + 1] = normal10;
                            normals[index + 2] = normal01;
                            normals[index + 3] = normal11;

                            colors32[index + 0] = vertexColor;
                            colors32[index + 1] = vertexColor;
                            colors32[index + 2] = vertexColor;
                            colors32[index + 3] = vertexColor;

                        }

                        // swap
                        {

                            //meter00 = meter01;
                            //meter10 = meter11;

                            pos00 = pos01;
                            pos10 = pos11;

                            normal00 = normal01;
                            normal10 = normal11;

                            width00to10 = width01to11;

                        }

                    }

                }

                // triangles
                {

                    Vector3 dirA = (vertices[1] - vertices[0]).normalized;
                    Vector3 dirB = (vertices[2] - vertices[0]).normalized;

                    bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                    if (!flip)
                    {

                        for (int v = 0, index = 0; v < numberOfCutsV - 1; v++, index += 4)
                        {

                            triangles[trianglesCounter++] = index + 0;
                            triangles[trianglesCounter++] = index + 2;
                            triangles[trianglesCounter++] = index + 1;

                            triangles[trianglesCounter++] = index + 1;
                            triangles[trianglesCounter++] = index + 2;
                            triangles[trianglesCounter++] = index + 3;

                        }

                    }

                    else
                    {

                        for (int v = 0, index = 0; v < numberOfCutsV - 1; v++, index += 4)
                        {

                            triangles[trianglesCounter++] = index + 0;
                            triangles[trianglesCounter++] = index + 1;
                            triangles[trianglesCounter++] = index + 2;

                            triangles[trianglesCounter++] = index + 1;
                            triangles[trianglesCounter++] = index + 3;
                            triangles[trianglesCounter++] = index + 2;

                        }

                    }

                }

            }

            // calcRoadLightmapUv
            {
                if (generateLightmapUv)
                {
                    this.calcRoadLightmapUv(vertices, uvs1);
                }
            }

            // set
            {

                dest.Clear();

                dest.vertices = vertices;
                dest.SetUVs(0, uvs0);
                dest.SetUVs(1, uvs1);
                dest.SetUVs(2, uvs2);
                dest.SetUVs(3, uvs3);
                dest.triangles = triangles;
                dest.normals = normals;
                dest.colors32 = colors32;

                dest.RecalculateBounds();

                //dest.RecalculateNormals();

                //#if UNITY_2018_1_OR_NEWER
                //                dest.RecalculateTangents();
                //#endif

                dest.UploadMeshData(false);

            }

        }

        /// <summary>
        /// Calculate lightmap uvs for a road
        /// </summary>
        /// <param name="vertices">vertices</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        protected virtual void calcRoadLightmapUv(Vector3[] vertices, List<Vector4> dest)
        {

            int size = vertices.Length;

            if (
                size != dest.Count ||
                size <= 3
                )
            {
                // Debug.LogError("vertices.Length != dest.Count in EquidistantBezierCurve.calcLightmapUv");
                return;
            }

            // -------------------

            float minX = 1000000.0f;
            float minZ = 1000000.0f;
            float maxX = -1000000.0f;
            float maxZ = -1000000.0f;

            // -------------------

            // calc
            {

                Vector3 pos00 = vertices[0];
                Vector3 pos10 = vertices[1];
                Vector3 pos01 = Vector3.zero;
                Vector3 pos11 = Vector3.zero;

                Vector3 currPosOnL = Vector3.zero;
                Vector3 currPosOnR = new Vector3((pos10 - pos00).magnitude, 0, 0);
                Vector3 nextPosOnL = Vector3.zero;
                Vector3 nextPosOnR = Vector3.zero;

                Vector3 vec00To10 = Vector3.right;
                Vector3 vec00To01 = Vector3.forward;
                Vector3 vec10To11 = Vector3.forward;

                Vector3 dir00To10 = Vector3.right;
                Vector3 dir00To01 = Vector3.forward;
                Vector3 dir10To11 = Vector3.forward;

                float distance00to01 = 0.0f;
                float distance10to11 = 0.0f;

                float angle00To01 = 0.0f;
                float angle10To11 = 0.0f;

                //
                {
                    minX = Mathf.Min(currPosOnL.x, currPosOnR.x, minX);
                    maxX = Mathf.Max(currPosOnL.x, currPosOnR.x, maxX);
                    minZ = Mathf.Min(currPosOnL.z, currPosOnR.z, minZ);
                    maxZ = Mathf.Max(currPosOnL.z, currPosOnR.z, maxZ);
                }

                for (int i = 0; i < vertices.Length; i += 4)
                {

                    pos00 = vertices[i + 0];
                    pos10 = vertices[i + 1];
                    pos01 = vertices[i + 2];
                    pos11 = vertices[i + 3];

                    vec00To10 = (pos10 - pos00);
                    vec00To01 = (pos01 - pos00);
                    vec10To11 = (pos11 - pos10);

                    angle00To01 = Vector3.SignedAngle(vec00To10, vec00To01, Vector3.up);
                    angle10To11 = Vector3.SignedAngle(vec00To10, vec10To11, Vector3.up);

                    distance00to01 = (pos01 - pos00).magnitude;
                    distance10to11 = (pos11 - pos10).magnitude;

                    dir00To10 = (currPosOnR - currPosOnL).normalized;
                    dir00To01 = Quaternion.AngleAxis(angle00To01, Vector3.up) * dir00To10;
                    dir10To11 = Quaternion.AngleAxis(angle10To11, Vector3.up) * dir00To10;

                    nextPosOnL = currPosOnL + (distance00to01 * dir00To01);
                    nextPosOnR = currPosOnR + (distance10to11 * dir10To11);

                    // --------------------

                    dest[i + 0] = new Vector4(currPosOnL.x, currPosOnL.z, dest[i + 0].z, dest[i + 0].w);
                    dest[i + 1] = new Vector4(currPosOnR.x, currPosOnR.z, dest[i + 1].z, dest[i + 1].w);
                    dest[i + 2] = new Vector4(nextPosOnL.x, nextPosOnL.z, dest[i + 2].z, dest[i + 2].w);
                    dest[i + 3] = new Vector4(nextPosOnR.x, nextPosOnR.z, dest[i + 3].z, dest[i + 3].w);

                    minX = Mathf.Min(nextPosOnL.x, nextPosOnR.x, minX);
                    maxX = Mathf.Max(nextPosOnL.x, nextPosOnR.x, maxX);
                    minZ = Mathf.Min(nextPosOnL.z, nextPosOnR.z, minZ);
                    maxZ = Mathf.Max(nextPosOnL.z, nextPosOnR.z, maxZ);

                    // --------------------

                    currPosOnL = nextPosOnL;
                    currPosOnR = nextPosOnR;

                }

            }

            // offset and scale
            {

                float diffX = (maxX - minX);
                float diffZ = (maxZ - minZ);


                if (
                    diffX > 0.0f &&
                    diffZ > 0.0f
                    )
                {

                    float offsetX = -minX;
                    float offsetZ = -minZ;
                    float scaleX = 1.0f / diffX;
                    float scaleZ = 1.0f / diffZ;

                    float minScale = Mathf.Min(scaleX, scaleZ);

                    Vector4 temp = Vector4.zero;

                    for (int i = 0; i < vertices.Length; i++)
                    {

                        temp = dest[i];

                        dest[i] = new Vector4(
                            (temp.x + offsetX) * minScale,
                            (temp.y + offsetZ) * minScale,
                            temp.z,
                            temp.w
                            );

                    }

                }

            }

        }







































        /// <summary>
        /// Calculate center point
        /// </summary>
        /// <param name="pos00">bottom left position</param>
        /// <param name="pos10">bottom right position</param>
        /// <param name="pos01">top left position</param>
        /// <param name="pos11">top right position</param>
        /// <returns>center point</returns>
        // -------------------------------------------------------------------------------------------
        protected Vector3 calcCenterPoint(
            Vector3 pos00,
            Vector3 pos10,
            Vector3 pos01,
            Vector3 pos11
            )
        {

            return Vector3.Lerp(
                Vector3.Lerp(pos00, pos10, 0.5f),
                Vector3.Lerp(pos01, pos11, 0.5f),
                0.5f
                );

        }

        /// <summary>
        /// Create simple road with other bezier
        /// </summary>
        /// <param name="otherBezier">other bezier</param>
        /// <param name="thisStartMeterFrom">this start point meter</param>
        /// <param name="thisEndMeterTo">this end point meter</param>
        /// <param name="otherStartMeterFrom">other start point meter</param>
        /// <param name="otherEndMeterTo">other end point meter</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="upSideDirection"up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="vertexColorFrom">vertex color at road starts</param>
        /// <param name="vertexColorTo">vertex color at road ends</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="thisStartUvX">this start uv X</param>
        /// <param name="thisEndUvX">this end uv X</param>
        /// <param name="otherStartUvX">other start uv X</param>
        /// <param name="otherEndUvX">other end uv X</param>
        [Obsolete("Not work well")]
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoadWithOtherBezierAsTris(
            EquidistantBezierCurve otherBezier,
            float thisStartMeterFrom,
            float thisEndMeterTo,
            float otherStartMeterFrom,
            float otherEndMeterTo,
            float repeatUvMeterV,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform,
            Color32 vertexColorFrom,
            Color32 vertexColorTo,
            int numberOfCutsV = -1,
            float thisStartUvX = 0.0f,
            float thisEndUvX = 0.0f,
            float otherStartUvX = 1.0f,
            float otherEndUvX = 1.0f
            )
        {

            // check
            {
                if (
                    otherBezier == null ||
                    !dest
                    )
                {
                    return;
                }
            }

            // -----------------------

            thisStartMeterFrom = Mathf.Max(0, thisStartMeterFrom);
            otherStartMeterFrom = Mathf.Max(0, otherStartMeterFrom);

            thisEndMeterTo = Mathf.Min(thisEndMeterTo, this.sampledTotalMeterLength);
            otherEndMeterTo = Mathf.Min(otherEndMeterTo, otherBezier.sampledTotalMeterLength);

            numberOfCutsV = Mathf.Max(2, (numberOfCutsV < 2) ? this.sampledPointCount : numberOfCutsV);

            int vertexCount = (2 * numberOfCutsV) + (numberOfCutsV - 1);

            Vector3[] vertices = new Vector3[vertexCount];
            List<Vector4> uvs = new List<Vector4>(new Vector4[vertexCount]);
            Vector3[] normals = new Vector3[vertexCount];
            Color32[] colors32 = new Color32[vertexCount];
            int[] triangles = new int[(numberOfCutsV - 1) * 3 * 4];

            float thisBezierLength = Mathf.Abs(thisEndMeterTo - thisStartMeterFrom);
            float otherBezierLength = Mathf.Abs(otherEndMeterTo - otherStartMeterFrom);

            float averageBezierLength = (thisBezierLength + otherBezierLength) * 0.5f;

            // ------------------------

            // vertices, uvs (straight along z)
            {

                Vector3 vertex = Vector3.zero;
                Vector4 uv = Vector4.one;

                int index = 0;
                int trianglesCounter = 0;
                int u = 0;

                float thisMeter = 0.0f;
                float otherMeter = 0.0f;
                float lerpValV = 0.0f;
                float lerpValU = 0.0f;
                float startUvX = 0.0f;
                float endUvX = 0.0f;
                float startUvY = 0.0f;
                float endUvY = Mathf.RoundToInt(averageBezierLength / repeatUvMeterV);

                Vector3 pointFrom = Vector3.zero;
                Vector3 pointTo = Vector3.zero;

                Vector3 normalFrom = Vector3.up;
                Vector3 normalTo = Vector3.up;

                Color32 vertexColor = new Color32(255, 255, 255, 255);

                // vertices, uvs, normals
                {

                    for (int v = 0; v < numberOfCutsV; v++)
                    {

                        lerpValV = v / (float)(numberOfCutsV - 1);

                        thisMeter = Mathf.Lerp(thisStartMeterFrom, thisEndMeterTo, lerpValV);
                        otherMeter = Mathf.Lerp(otherStartMeterFrom, otherEndMeterTo, lerpValV);

                        pointFrom =
                            (useSampledPoints) ?
                            this.findSampledPointByMeter(thisMeter) :
                            this.findRawBezierPoint(thisMeter)
                            ;

                        pointTo =
                            (useSampledPoints) ?
                            otherBezier.findSampledPointByMeter(otherMeter) :
                            otherBezier.findRawBezierPoint(otherMeter)
                            ;

                        uv.y = Mathf.Lerp(startUvY, endUvY, lerpValV);

                        startUvX = Mathf.Lerp(thisStartUvX, thisEndUvX, lerpValV);
                        endUvX = Mathf.Lerp(otherStartUvX, otherEndUvX, lerpValV);

                        normalFrom = Vector3.Lerp(
                            this.startUpwards,
                            this.endUpwards,
                            (this.sampledTotalMeterLength > 0.0f) ? thisMeter / this.sampledTotalMeterLength : 0.0f)
                            .normalized
                            ;

                        normalTo = Vector3.Lerp(
                            otherBezier.startUpwards,
                            otherBezier.endUpwards,
                            (otherBezier.sampledTotalMeterLength > 0.0f) ? otherMeter / otherBezier.sampledTotalMeterLength : 0.0f)
                            .normalized
                            ;

                        vertexColor = Color32.Lerp(vertexColorFrom, vertexColorTo, lerpValV);

                        for (u = 0; u < 2; u++)
                        {

                            lerpValU = u;

                            vertex =
                                (baseTransform) ?
                                baseTransform.InverseTransformPoint(Vector3.Lerp(pointFrom, pointTo, lerpValU)) :
                                Vector3.Lerp(pointFrom, pointTo, lerpValU)
                                ;

                            uv.x = Mathf.Lerp(startUvX, endUvX, lerpValU);

                            vertices[index] = vertex;

                            uvs[index] = new Vector4(uv.x, uv.y, 0.0f, 0.0f);
                            normals[index] = Vector3.Lerp(normalFrom, normalTo, lerpValU).normalized;
                            colors32[index] = vertexColor;

                            index++;

                        }

                    }

                    for (int v = 0, v2 = 0; v < numberOfCutsV - 1; v++, v2 += 2)
                    {

                        lerpValV = v / (float)(numberOfCutsV - 1);

                        vertices[index] = this.calcCenterPoint(
                            vertices[v2 + 0],
                            vertices[v2 + 1],
                            vertices[v2 + 2],
                            vertices[v2 + 3]
                            );

                        uvs[index] = Vector4.Lerp(uvs[v2 + 0], uvs[v2 + 3], 0.5f);
                        normals[index] = Vector3.Lerp(normals[v2 + 0], normals[v2 + 3], 0.5f);
                        colors32[index] = Color32.Lerp(colors32[v2 + 0], colors32[v2 + 3], 0.5f);

                        index++;

                    }

                }

                // triangles
                {

                    Vector3 dirA = (vertices[1] - vertices[0]).normalized;
                    Vector3 dirB = (vertices[2] - vertices[0]).normalized;

                    bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                    if (!flip)
                    {

                        for (int v = 0, v2 = 0, centerIndex = numberOfCutsV * 2; v < numberOfCutsV - 1; v++, v2 += 2, centerIndex++)
                        {

                            triangles[trianglesCounter++] = v2 + 0;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 1;

                            triangles[trianglesCounter++] = v2 + 2;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 0;

                            triangles[trianglesCounter++] = v2 + 3;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 2;

                            triangles[trianglesCounter++] = v2 + 1;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 3;

                        }

                    }

                    else
                    {

                        for (int v = 0, v2 = 0, centerIndex = numberOfCutsV * 2; v < numberOfCutsV - 1; v++, v2 += 2, centerIndex++)
                        {

                            triangles[trianglesCounter++] = v2 + 1;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 0;

                            triangles[trianglesCounter++] = v2 + 0;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 2;

                            triangles[trianglesCounter++] = v2 + 2;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 3;

                            triangles[trianglesCounter++] = v2 + 3;
                            triangles[trianglesCounter++] = centerIndex;
                            triangles[trianglesCounter++] = v2 + 1;

                        }

                    }

                }

            }

            // set
            {

                dest.Clear();

                dest.vertices = vertices;
                dest.SetUVs(0, uvs);
                dest.triangles = triangles;
                dest.normals = normals;
                dest.colors32 = colors32;

                dest.RecalculateBounds();
                //dest.RecalculateNormals();

                //#if UNITY_2018_1_OR_NEWER
                //                dest.RecalculateTangents();
                //#endif

                dest.UploadMeshData(false);

            }

        }

    }

}
