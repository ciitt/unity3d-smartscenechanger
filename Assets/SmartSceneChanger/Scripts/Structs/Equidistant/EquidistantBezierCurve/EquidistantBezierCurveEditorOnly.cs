﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Equidistant bezier curve
    /// </summary>
    public partial class EquidistantBezierCurve : EquidistantPointList
    {

        /// <summary>
        /// Draw debug bezier line (EditorOnly)
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierLineEditorOnly()
        {
            this.drawDebugBezierLineEditorOnly(Color.yellow, FuncsEditorOnly.PolyLineWidth);
        }

        /// <summary>
        /// Draw debug bezier line (EditorOnly)
        /// </summary>
        /// <param name="color">line color</param>
        /// <param name="width">line width</param>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierLineEditorOnly(Color color, float width)
        {

            Handles.DrawBezier(
                this.p0,
                this.p3,
                this.p1,
                this.p2,
                color,
                null,
                width
                );

        }

        /// <summary>
        /// Draw debug bezier point lines (EditorOnly)
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierPointLinesEditorOnly()
        {
            this.drawDebugBezierPointLinesEditorOnly(Color.cyan);
        }

        /// <summary>
        /// Draw debug bezier point lines (EditorOnly)
        /// </summary>
        /// <param name="color">line color</param>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierPointLinesEditorOnly(Color color)
        {

            Handles.color = color;
            Handles.DrawLine(this.p0, this.p1);
            Handles.DrawLine(this.p1, this.p2);
            Handles.DrawLine(this.p2, this.p3);

        }

        /// <summary>
        /// Draw debug bezier position info (EditorOnly)
        /// </summary>
        /// <param name="position01">normalized position</param>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierPositionInfoEditorOnly(float position01)
        {

            float lengthScale = 5.0f;

            // ------------------------

            // findSampledBezierPointByNormalizedValue
            {
                this.findSampledBezierPointByNormalizedValue(position01, TempPobForAny);
            }

            // tangent
            {

                Handles.color = Handles.zAxisColor;

                Handles.DrawAAPolyLine(
                    FuncsEditorOnly.PolyLineWidth,
                    TempPobForAny.position,
                    TempPobForAny.position + (TempPobForAny.tangent * lengthScale)
                    );

            }

            // normal
            {

                Handles.color = Handles.xAxisColor;

                Handles.DrawAAPolyLine(
                    FuncsEditorOnly.PolyLineWidth,
                    TempPobForAny.position,
                    TempPobForAny.position + (TempPobForAny.normalRight * lengthScale)
                    );

            }

            // up
            {

                Handles.color = Handles.yAxisColor;

                Handles.DrawAAPolyLine(
                    FuncsEditorOnly.PolyLineWidth,
                    TempPobForAny.position,
                    TempPobForAny.position + (TempPobForAny.up * lengthScale)
                    );

            }

        }

    }

}

#endif
