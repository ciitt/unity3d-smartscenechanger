﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Equidistant bezier curve
    /// </summary>
    public partial class EquidistantBezierCurve : EquidistantPointList
    {

        /// <summary>
        /// Create simple road
        /// </summary>
        /// <param name="width0">road width 0</param>
        /// <param name="width1">road width 1</param>
        /// <param name="startMeterFrom">start meter</param>
        /// <param name="endMeterTo">end meter</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="dest">destination</param>
        /// <param name="baseTransform">base Transform</param>
        [Obsolete("Use another createSimpleRoad", false)]
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoad(
            float width0,
            float width1,
            float startMeterFrom,
            float endMeterTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            Mesh dest,
            Transform baseTransform
            )
        {

            this.createSimpleRoad(
                width0,
                width1,
                startMeterFrom,
                endMeterTo,
                numberOfCutsU,
                repeatUvMeterV,
                useUvPerspectiveCorrection,
                startOffsetMeter,
                endOffsetMeter,
                dest,
                true,
                true,
                baseTransform
                );

        }

        /// <summary>
        /// Deform mesh along this bezier curve
        /// </summary>
        /// <param name="sourceAndDest">source and destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="baseTransform">base Transform</param>
        [Obsolete("Use another deformMeshAlongBezier", false)]
        // -------------------------------------------------------------------------------------------
        public virtual void deformMeshAlongBezier(
            Mesh sourceAndDest,
            float start01,
            float end01,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            Transform baseTransform
            )
        {

            this.deformMeshAlongBezier(
                sourceAndDest,
                start01,
                end01,
                startOffsetMeter,
                endOffsetMeter,
                true,
                baseTransform
                );

        }

        /// <summary>
        /// Copy and deform mesh along the bezier burve
        /// </summary>
        /// <param name="chunkOriginMesh">chunk</param>
        /// <param name="dest">destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="spacingMeter">spacing meter</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="moveUvY">move uv.y</param>
        /// <param name="baseTransform">base Transform</param>
        [Obsolete("Use another copyAndDeformMeshAlongBezier", false)]
        // -------------------------------------------------------------------------------------------
        public virtual void copyAndDeformMeshAlongBezier(
            Mesh chunkOriginMesh,
            Mesh dest,
            float start01,
            float end01,
            float spacingMeter,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            bool moveUvY,
            Transform baseTransform = null
            )
        {

            this.copyAndDeformMeshAlongBezier(
                chunkOriginMesh,
                dest,
                start01,
                end01,
                spacingMeter,
                startOffsetMeter,
                endOffsetMeter,
                moveUvY,
                true,
                baseTransform
                );

        }

        /// <summary>
        /// Copy and deform mesh along the bezier burve
        /// </summary>
        /// <param name="chunkOriginMeshAtStart">chunk mesh at start</param>
        /// <param name="chunkOriginMeshMainLoop">chunk mesh for main loop</param>
        /// <param name="chunkOriginMeshAtEnd">chunk mesh at end</param>
        /// <param name="dest">destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="spacingMeter">spacing meter</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="moveUvY">move uv.y</param>
        /// <param name="baseTransform">base Transform</param>
        [Obsolete("Use another copyAndDeformMeshAlongBezier", false)]
        // -------------------------------------------------------------------------------------------
        public virtual void copyAndDeformMeshAlongBezier(
            Mesh chunkOriginMeshAtStart,
            Mesh chunkOriginMeshMainLoop,
            Mesh chunkOriginMeshAtEnd,
            Mesh dest,
            float start01,
            float end01,
            float spacingMeter,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            bool moveUvY,
            Transform baseTransform = null
            )
        {

            this.copyAndDeformMeshAlongBezier(
                chunkOriginMeshAtStart,
                chunkOriginMeshMainLoop,
                chunkOriginMeshAtEnd,
                dest,
                start01,
                end01,
                spacingMeter,
                startOffsetMeter,
                endOffsetMeter,
                moveUvY,
                true,
                baseTransform
                );

        }

    }

}

