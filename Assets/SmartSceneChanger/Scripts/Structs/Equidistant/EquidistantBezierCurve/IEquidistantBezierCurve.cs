﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Interface for EquidistantBezierCurve
    /// </summary>
    public interface IEquidistantBezierCurve
    {

        EquidistantBezierCurve bezier
        {
            get;
            set;
        }

    }

}
