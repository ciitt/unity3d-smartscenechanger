﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Equidistant bezier curve
    /// </summary>
    public partial class EquidistantBezierCurve : EquidistantPointList
    {

        /// <summary>
        /// Create simple road
        /// </summary>
        /// <param name="width0">road width 0</param>
        /// <param name="width1">road width 1</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoad(
            float width0,
            float width1,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest
            )
        {

            this.createSimpleRoad(
                width0,
                width1,
                0,
                this.sampledTotalMeterLength,
                numberOfCutsU,
                repeatUvMeterV,
                useUvPerspectiveCorrection,
                Vector2.zero,
                Vector2.zero,
                dest,
                true,
                true,
                null
                );

        }

        /// <summary>
        /// Create simple road
        /// </summary>
        /// <param name="width0">road width 0</param>
        /// <param name="width1">road width 1</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoad(
            float width0,
            float width1,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest,
            Transform baseTransform
            )
        {

            this.createSimpleRoad(
                width0,
                width1,
                0,
                this.sampledTotalMeterLength,
                numberOfCutsU,
                repeatUvMeterV,
                useUvPerspectiveCorrection,
                Vector2.zero,
                Vector2.zero,
                dest,
                true,
                true,
                baseTransform
                );

        }

        /// <summary>
        /// Create simple road
        /// </summary>
        /// <param name="width0">road width 0</param>
        /// <param name="width1">road width 1</param>
        /// <param name="startMeterFrom">start meter</param>
        /// <param name="endMeterTo">end meter</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoad(
            float width0,
            float width1,
            float startMeterFrom,
            float endMeterTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest,
            Transform baseTransform
            )
        {

            this.createSimpleRoad(
                width0,
                width1,
                startMeterFrom,
                endMeterTo,
                numberOfCutsU,
                repeatUvMeterV,
                useUvPerspectiveCorrection,
                Vector2.zero,
                Vector2.zero,
                dest,
                true,
                true,
                baseTransform
                );

        }

        /// <summary>
        /// Create simple road
        /// </summary>
        /// <param name="width0">road width 0</param>
        /// <param name="width1">road width 1</param>
        /// <param name="startMeterFrom">start meter</param>
        /// <param name="endMeterTo">end meter</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="useSmoothLerp">use smooth lerp</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoad(
            float width0,
            float width1,
            float startMeterFrom,
            float endMeterTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            Mesh dest,
            bool useSampledPoints,
            bool useSmoothLerp,
            Transform baseTransform
            )
        {

            if (
                !dest ||
                width0 <= 0.0f ||
                width1 <= 0.0f ||
                this.sampledPointCount < 2 ||
                this.sampledTotalMeterLength <= 0.0f
                )
            {
                return;
            }

            // ------------------------

            numberOfCutsU = Mathf.Max(2, numberOfCutsU);

            int numberOfCutsV = this.sampledPointCount;

            Vector3[] vertices = new Vector3[numberOfCutsU * numberOfCutsV];
            List<Vector4> uvs = new List<Vector4>(new Vector4[numberOfCutsU * numberOfCutsV]);
            Vector3[] normals = new Vector3[numberOfCutsU * numberOfCutsV];
            Color32[] colors32 = new Color32[numberOfCutsU * numberOfCutsV];
            int[] triangles = new int[(numberOfCutsU - 1) * (numberOfCutsV - 1) * 6];

            // ------------------------

            // vertices, uvs (straight along z)
            {

                Vector3 vertex = Vector3.zero;
                Vector4 uv = Vector4.one;

                Vector2 uvTo =
                    (repeatUvMeterV > 0.0f) ?
                    new Vector2(1, Mathf.RoundToInt(this.sampledTotalMeterLength / repeatUvMeterV)) :
                    Vector2.one
                    ;

                int index = 0;
                int trianglesCounter = 0;
                int u = 0;

                float lerpValV = 0.0f;
                float lerpValU = 0.0f;
                float width = 0.0f;
                float halfWidth = 0.0f;
                float uvCorrectionX = 1.0f;

                Color32 vertexColor = new Color32(255, 255, 255, 255);
                Color32 vertexColorFrom = new Color32(255, 255, 255, 0);
                Color32 vertexColorTo = new Color32(255, 255, 255, 255);

                for (int v = 0; v < numberOfCutsV; v++)
                {

                    lerpValV = v / (float)(numberOfCutsV - 1);

                    //width = Mathf.Lerp(width0, width1, this.smoothLerpVal(lerpValV));
                    width = Mathf.Lerp(
                        width0,
                        width1,
                        (useSmoothLerp) ? this.smoothLerpVal(lerpValV) : lerpValV
                        );

                    halfWidth = width * 0.5f;

                    vertex.z = Mathf.Lerp(0, this.sampledTotalMeterLength, lerpValV);
                    uv.y = Mathf.Lerp(0.0f, uvTo.y, lerpValV);

                    uvCorrectionX = (useUvPerspectiveCorrection) ? width : 1.0f;

                    vertexColor = Color32.Lerp(vertexColorFrom, vertexColorTo, lerpValV);
                    
                    for (u = 0; u < numberOfCutsU; u++)
                    {

                        lerpValU = u / (float)(numberOfCutsU - 1);

                        vertex.x = Mathf.Lerp(-halfWidth, halfWidth, lerpValU);
                        uv.x = Mathf.Lerp(0.0f, uvTo.x, lerpValU);

                        index = u + (v * numberOfCutsU);

                        vertices[index] = vertex;

                        uvs[index] = new Vector4(uv.x * uvCorrectionX, uv.y, uvCorrectionX, 1);
                        normals[index] = Vector3.up;
                        colors32[index] = vertexColor;

                    }

                }

                for (int v = 0; v < numberOfCutsV - 1; v++)
                {

                    for (u = 0; u < numberOfCutsU - 1; u++)
                    {

                        index = u + (v * numberOfCutsU);

                        triangles[trianglesCounter++] = index + 0;
                        triangles[trianglesCounter++] = index + numberOfCutsU;
                        triangles[trianglesCounter++] = index + 1;

                        triangles[trianglesCounter++] = index + 1;
                        triangles[trianglesCounter++] = index + numberOfCutsU;
                        triangles[trianglesCounter++] = index + numberOfCutsU + 1;

                    }

                }

            }

            // deform vertices, normals
            {

                float lerpVal = 0.0f;

                startMeterFrom = Mathf.Clamp(startMeterFrom, 0, this.sampledTotalMeterLength);
                endMeterTo = Mathf.Clamp(endMeterTo, 0, this.sampledTotalMeterLength);

                float remapFactorMultiply = (endMeterTo - startMeterFrom) / this.sampledTotalMeterLength;
                float remapFactorPlus = startMeterFrom / this.sampledTotalMeterLength;

                Vector3 offsetMeter = Vector3.zero;

                Vector4 uv = Vector4.zero;
                float distance = 0.0f;

                // deform
                {

                    for (int i = 0; i < vertices.Length; i++)
                    {

                        lerpVal = Mathf.InverseLerp(0, this.sampledTotalMeterLength, vertices[i].z);
                        lerpVal = (lerpVal * remapFactorMultiply) + remapFactorPlus;

                        offsetMeter = new Vector3(
                            Mathf.Lerp(startOffsetMeter.x, endOffsetMeter.x, lerpVal),
                            Mathf.Lerp(startOffsetMeter.y, endOffsetMeter.y, lerpVal),
                            0
                            );

                        if (useSampledPoints)
                        {
                            this.findSampledBezierPointByNormalizedValue(lerpVal, TempPobForAny);
                        }

                        else
                        {
                            this.findRawBezierPoint(lerpVal, TempPobForAny);
                        }

                        vertices[i] =
                            TempPobForAny.position +
                            ((vertices[i].x + offsetMeter.x) * TempPobForAny.normalRight) +
                            ((vertices[i].y + offsetMeter.y) * TempPobForAny.up)
                            ;

                        normals[i] = TempPobForAny.up;

                        if (useUvPerspectiveCorrection && (i >= numberOfCutsU))
                        {

                            distance = Vector3.Distance(vertices[i], vertices[i - numberOfCutsU]);

                            uv = uvs[i];
                            uv.y = uv.y * distance;
                            uv.w = distance;
                            uvs[i] = uv;

                        }

                    }

                    if (useUvPerspectiveCorrection)
                    {

                        for (int i = 0; i < numberOfCutsU; i++)
                        {

                            distance = Vector3.Distance(vertices[i], vertices[i + numberOfCutsU]);

                            uv = uvs[i];
                            uv.y = uv.y * distance;
                            uv.w = distance;
                            uvs[i] = uv;

                        }

                    }

                }

            }

            // InverseTransformPoint
            {

                if (baseTransform)
                {

                    for (int i = vertices.Length - 1; i >= 0; i--)
                    {
                        vertices[i] = baseTransform.InverseTransformPoint(vertices[i]);
                    }

                }

            }

            // set
            {

                dest.Clear();

                dest.vertices = vertices;
                dest.SetUVs(0, uvs);
                dest.triangles = triangles;
                dest.normals = normals;
                dest.colors32 = colors32;

                dest.RecalculateBounds();
                //dest.RecalculateNormals();

//#if UNITY_2018_1_OR_NEWER
//                dest.RecalculateTangents();
//#endif

                dest.UploadMeshData(false);

            }

        }

        /// <summary>
        /// Deform mesh along this bezier curve
        /// </summary>
        /// <param name="sourceAndDest">source and destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void deformMeshAlongBezier(
            Mesh sourceAndDest,
            float start01,
            float end01,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            bool useSampledPoints,
            Transform baseTransform
            )
        {

            this.deformMeshAlongBezier(
                sourceAndDest,
                start01,
                end01,
                startOffsetMeter,
                endOffsetMeter,
                useSampledPoints,
                true,
                baseTransform
                );

        }

        /// <summary>
        /// Deform mesh along this bezier curve
        /// </summary>
        /// <param name="sourceAndDest">source and destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="useSmoothLerp">use smooth lerp</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void deformMeshAlongBezier(
            Mesh sourceAndDest,
            float start01,
            float end01,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            bool useSampledPoints,
            bool useSmoothLerp,
            Transform baseTransform
            )
        {

            if (!sourceAndDest)
            {
                return;
            }

            // ------------------------

            Bounds bounds = sourceAndDest.bounds;

            float minZ = bounds.min.z;
            float maxZ = bounds.max.z;

            Vector3[] vertices = sourceAndDest.vertices;

            // ------------------------

            //
            {

                float lerpVal = 0.0f;

                Vector2 offsetMeter = Vector2.zero;

                for (int i = vertices.Length - 1; i >= 0; i--)
                {

                    lerpVal = Mathf.Lerp(start01, end01, Mathf.InverseLerp(minZ, maxZ, vertices[i].z));

                    if (useSampledPoints)
                    {
                        this.findSampledBezierPointByNormalizedValue(lerpVal, TempPobForAny);
                    }

                    else
                    {
                        this.findRawBezierPoint(lerpVal, TempPobForAny);
                    }

                    offsetMeter = Vector2.Lerp(
                        startOffsetMeter,
                        endOffsetMeter,
                        (useSmoothLerp) ? this.smoothLerpVal(lerpVal) : lerpVal
                        );

                    if (baseTransform)
                    {

                        vertices[i] = baseTransform.InverseTransformPoint(
                            TempPobForAny.position +
                            ((vertices[i].x + offsetMeter.x) * TempPobForAny.normalRight) +
                            ((vertices[i].y + offsetMeter.y) * TempPobForAny.up)
                            );

                    }

                    else
                    {

                        vertices[i] =
                            TempPobForAny.position +
                            ((vertices[i].x + offsetMeter.x) * TempPobForAny.normalRight) +
                            ((vertices[i].y + offsetMeter.y) * TempPobForAny.up)
                            ;

                    }

                }

            }

            // UploadMeshData
            {

                sourceAndDest.vertices = vertices;

                sourceAndDest.RecalculateBounds();
                sourceAndDest.RecalculateNormals();

//#if UNITY_2018_1_OR_NEWER
//                sourceAndDest.RecalculateTangents();
//#endif

                sourceAndDest.UploadMeshData(false);

            }

        }

        /// <summary>
        /// Copy and deform mesh along the bezier burve
        /// </summary>
        /// <param name="chunkOriginMesh">chunk</param>
        /// <param name="dest">destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="spacingMeter">spacing meter</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="moveUvY">move uv.y</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="useSmoothLerp">use smooth lerp</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void copyAndDeformMeshAlongBezier(
            Mesh chunkOriginMesh,
            Mesh dest,
            float start01,
            float end01,
            float spacingMeter,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            bool moveUvY,
            bool useSampledPoints,
            bool useSmoothLerp,
            Transform baseTransform = null
            )
        {

            if (!chunkOriginMesh || !dest)
            {
                return;
            }

            // ------------------------

            // CopyMeshAlongZ, deformMeshAlongBezier
            {

                if (Funcs.CopyMeshAlongZ(
                    chunkOriginMesh,
                    dest,
                    this.sampledTotalMeterLength * (end01 - start01),
                    spacingMeter,
                    moveUvY
                    ))
                {
                    this.deformMeshAlongBezier(dest, start01, end01, startOffsetMeter, endOffsetMeter, useSampledPoints, useSmoothLerp, baseTransform);
                }

            }

        }

        /// <summary>
        /// Copy and deform mesh along the bezier burve
        /// </summary>
        /// <param name="chunkOriginMeshAtStart">chunk mesh at start</param>
        /// <param name="chunkOriginMeshMainLoop">chunk mesh for main loop</param>
        /// <param name="chunkOriginMeshAtEnd">chunk mesh at end</param>
        /// <param name="dest">destination</param>
        /// <param name="start01">start</param>
        /// <param name="end01">end</param>
        /// <param name="spacingMeter">spacing meter</param>
        /// <param name="startOffsetMeter">offset meter at start</param>
        /// <param name="endOffsetMeter">offset meter at end</param>
        /// <param name="moveUvY">move uv.y</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="useSmoothLerp">use smooth lerp</param>
        /// <param name="baseTransform">base Transform</param>
        // -------------------------------------------------------------------------------------------
        public virtual void copyAndDeformMeshAlongBezier(
            Mesh chunkOriginMeshAtStart,
            Mesh chunkOriginMeshMainLoop,
            Mesh chunkOriginMeshAtEnd,
            Mesh dest,
            float start01,
            float end01,
            float spacingMeter,
            Vector2 startOffsetMeter,
            Vector2 endOffsetMeter,
            bool moveUvY,
            bool useSampledPoints,
            bool useSmoothLerp,
            Transform baseTransform = null
            )
        {

            if (
                !chunkOriginMeshAtStart ||
                !chunkOriginMeshMainLoop ||
                !chunkOriginMeshAtEnd ||
                !dest
                )
            {
                return;
            }

            // ------------------------

            // CopyMeshAlongZ, deformMeshAlongBezier
            {

                if (Funcs.CopyMeshAlongZ(
                    chunkOriginMeshAtStart,
                    chunkOriginMeshMainLoop,
                    chunkOriginMeshAtEnd,
                    dest,
                    this.sampledTotalMeterLength * (end01 - start01),
                    spacingMeter,
                    moveUvY
                    ))
                {
                    this.deformMeshAlongBezier(dest, start01, end01, startOffsetMeter, endOffsetMeter, useSampledPoints, useSmoothLerp, baseTransform);
                }

            }

        }

        /// <summary>
        /// Create simple road with other bezier
        /// </summary>
        /// <param name="otherBezier">other bezier</param>
        /// <param name="thisStartNormalizedFrom">this start normalized point</param>
        /// <param name="thisEndNormalizedTo">this end normalized point</param>
        /// <param name="otherStartNormalizedFrom">other start normalized point</param>
        /// <param name="otherEndNormalizedTo">other end normalized point</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="thisStartUvX">this start uv X</param>
        /// <param name="thisEndUvX">this end uv X</param>
        /// <param name="otherStartUvX">other start uv X</param>
        /// <param name="otherEndUvX">other end uv X</param>
        [Obsolete("Use createSimpleRoadWithOtherBezierVer2")]
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoadWithOtherBezierN(
            EquidistantBezierCurve otherBezier,
            float thisStartNormalizedFrom,
            float thisEndNormalizedTo,
            float otherStartNormalizedFrom,
            float otherEndNormalizedTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform,
            int numberOfCutsV = -1,
            float thisStartUvX = 0.0f,
            float thisEndUvX = 0.0f,
            float otherStartUvX = 1.0f,
            float otherEndUvX = 1.0f
            )
        {

            this.createSimpleRoadWithOtherBezierN(
                otherBezier,
                thisStartNormalizedFrom,
                thisEndNormalizedTo,
                otherStartNormalizedFrom,
                otherEndNormalizedTo,
                numberOfCutsU,
                repeatUvMeterV,
                useUvPerspectiveCorrection,
                dest,
                useSampledPoints,
                upSideDirection,
                baseTransform,
                new Color32(255, 255, 255, 0),
                new Color32(255, 255, 255, 255),
                numberOfCutsV,
                thisStartUvX,
                thisEndUvX,
                otherStartUvX,
                otherEndUvX
                );

        }

        /// <summary>
        /// Create simple road with other bezier
        /// </summary>
        /// <param name="otherBezier">other bezier</param>
        /// <param name="thisStartNormalizedFrom">this start normalized point</param>
        /// <param name="thisEndNormalizedTo">this end normalized point</param>
        /// <param name="otherStartNormalizedFrom">other start normalized point</param>
        /// <param name="otherEndNormalizedTo">other end normalized point</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="vertexColorFrom">vertex color at road starts</param>
        /// <param name="vertexColorTo">vertex color at road ends</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="thisStartUvX">this start uv X</param>
        /// <param name="thisEndUvX">this end uv X</param>
        /// <param name="otherStartUvX">other start uv X</param>
        /// <param name="otherEndUvX">other end uv X</param>
        [Obsolete("Use createSimpleRoadWithOtherBezierVer2")]
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoadWithOtherBezierN(
            EquidistantBezierCurve otherBezier,
            float thisStartNormalizedFrom,
            float thisEndNormalizedTo,
            float otherStartNormalizedFrom,
            float otherEndNormalizedTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform,
            Color32 vertexColorFrom,
            Color32 vertexColorTo,
            int numberOfCutsV = -1,
            float thisStartUvX = 0.0f,
            float thisEndUvX = 0.0f,
            float otherStartUvX = 1.0f,
            float otherEndUvX = 1.0f
            )
        {

            if (otherBezier != null)
            {

                this.createSimpleRoadWithOtherBezier(
                    otherBezier,
                    thisStartNormalizedFrom * this.sampledTotalMeterLength,
                    thisEndNormalizedTo * this.sampledTotalMeterLength,
                    otherStartNormalizedFrom * otherBezier.sampledTotalMeterLength,
                    otherEndNormalizedTo * otherBezier.sampledTotalMeterLength,
                    numberOfCutsU,
                    repeatUvMeterV,
                    useUvPerspectiveCorrection,
                    dest,
                    useSampledPoints,
                    upSideDirection,
                    baseTransform,
                    vertexColorFrom,
                    vertexColorTo,
                    numberOfCutsV,
                    thisStartUvX,
                    thisEndUvX,
                    otherStartUvX,
                    otherEndUvX
                    );

            }

        }

        /// <summary>
        /// Create simple road with other bezier
        /// </summary>
        /// <param name="otherBezier">other bezier</param>
        /// <param name="thisStartMeterFrom">this start point meter</param>
        /// <param name="thisEndMeterTo">this end point meter</param>
        /// <param name="otherStartMeterFrom">other start point meter</param>
        /// <param name="otherEndMeterTo">other end point meter</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="thisStartUvX">this start uv X</param>
        /// <param name="thisEndUvX">this end uv X</param>
        /// <param name="otherStartUvX">other start uv X</param>
        /// <param name="otherEndUvX">other end uv X</param>
        [Obsolete("Use createSimpleRoadWithOtherBezierVer2")]
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoadWithOtherBezier(
            EquidistantBezierCurve otherBezier,
            float thisStartMeterFrom,
            float thisEndMeterTo,
            float otherStartMeterFrom,
            float otherEndMeterTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform,
            int numberOfCutsV = -1,
            float thisStartUvX = 0.0f,
            float thisEndUvX = 0.0f,
            float otherStartUvX = 1.0f,
            float otherEndUvX = 1.0f
            )
        {

            this.createSimpleRoadWithOtherBezier(
                otherBezier,
                thisStartMeterFrom,
                thisEndMeterTo,
                otherStartMeterFrom,
                otherEndMeterTo,
                numberOfCutsU,
                repeatUvMeterV,
                useUvPerspectiveCorrection,
                dest,
                useSampledPoints,
                upSideDirection,
                baseTransform,
                new Color32(255, 255, 255, 0),
                new Color32(255, 255, 255, 255),
                numberOfCutsV,
                thisStartUvX,
                thisEndUvX,
                otherStartUvX,
                otherEndUvX
                );

        }

        /// <summary>
        /// Create simple road with other bezier
        /// </summary>
        /// <param name="otherBezier">other bezier</param>
        /// <param name="thisStartMeterFrom">this start point meter</param>
        /// <param name="thisEndMeterTo">this end point meter</param>
        /// <param name="otherStartMeterFrom">other start point meter</param>
        /// <param name="otherEndMeterTo">other end point meter</param>
        /// <param name="numberOfCutsU">number of cuts U</param>
        /// <param name="repeatUvMeterV">repeat uv meter for V</param>
        /// <param name="useUvPerspectiveCorrection">use uv perspective correction [tex2D (_MainTex, uv.xy / uv.zw)]</param>
        /// <param name="dest">destination</param>
        /// <param name="useSampledPoints">use sampled points</param>
        /// <param name="upSideDirection">up side direction</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="vertexColorFrom">vertex color at road starts</param>
        /// <param name="vertexColorTo">vertex color at road ends</param>
        /// <param name="numberOfCutsV">number of cuts V</param>
        /// <param name="thisStartUvX">this start uv X</param>
        /// <param name="thisEndUvX">this end uv X</param>
        /// <param name="otherStartUvX">other start uv X</param>
        /// <param name="otherEndUvX">other end uv X</param>
        [Obsolete("Use createSimpleRoadWithOtherBezierVer2")]
        // -------------------------------------------------------------------------------------------
        public virtual void createSimpleRoadWithOtherBezier(
            EquidistantBezierCurve otherBezier,
            float thisStartMeterFrom,
            float thisEndMeterTo,
            float otherStartMeterFrom,
            float otherEndMeterTo,
            int numberOfCutsU,
            float repeatUvMeterV,
            bool useUvPerspectiveCorrection,
            Mesh dest,
            bool useSampledPoints,
            Vector3 upSideDirection,
            Transform baseTransform,
            Color32 vertexColorFrom,
            Color32 vertexColorTo,
            int numberOfCutsV = -1,
            float thisStartUvX = 0.0f,
            float thisEndUvX = 0.0f,
            float otherStartUvX = 1.0f,
            float otherEndUvX = 1.0f
            )
        {

            // check
            {
                if (
                    otherBezier == null ||
                    !dest
                    )
                {
                    return;
                }
            }

            // -----------------------

            thisStartMeterFrom = Mathf.Max(0, thisStartMeterFrom);
            otherStartMeterFrom = Mathf.Max(0, otherStartMeterFrom);

            thisEndMeterTo = Mathf.Min(thisEndMeterTo, this.sampledTotalMeterLength);
            otherEndMeterTo = Mathf.Min(otherEndMeterTo, otherBezier.sampledTotalMeterLength);

            numberOfCutsU = Mathf.Max(2, numberOfCutsU);
            numberOfCutsV = Mathf.Max(2, (numberOfCutsV < 2) ? this.sampledPointCount : numberOfCutsV);

            Vector3[] vertices = new Vector3[numberOfCutsU * numberOfCutsV];
            List<Vector4> uvs = new List<Vector4>(new Vector4[numberOfCutsU * numberOfCutsV]);
            Vector3[] normals = new Vector3[numberOfCutsU * numberOfCutsV];
            Color32[] colors32 = new Color32[numberOfCutsU * numberOfCutsV];
            int[] triangles = new int[(numberOfCutsU - 1) * (numberOfCutsV - 1) * 6];

            float thisBezierLength = Mathf.Abs(thisEndMeterTo - thisStartMeterFrom);
            float otherBezierLength = Mathf.Abs(otherEndMeterTo - otherStartMeterFrom);

            float averageBezierLength = (thisBezierLength + otherBezierLength) * 0.5f;

            // ------------------------

            // vertices, uvs (straight along z)
            {

                Vector3 vertex = Vector3.zero;
                Vector4 uv = Vector4.one;

                int index = 0;
                int trianglesCounter = 0;
                int u = 0;

                float thisMeter = 0.0f;
                float otherMeter = 0.0f;
                float lerpValV = 0.0f;
                float lerpValU = 0.0f;
                float width = 0.0f;
                float uvCorrectionX = 1.0f;
                float uvCorrectionY = 1.0f;
                float startUvX = 0.0f;
                float endUvX = 0.0f;
                float startUvY = 0.0f;
                float endUvY = Mathf.RoundToInt(averageBezierLength / repeatUvMeterV);

                Vector3 pointFrom = Vector3.zero;
                Vector3 pointTo = Vector3.zero;

                Vector3 normalFrom = Vector3.up;
                Vector3 normalTo = Vector3.up;

                Color32 vertexColor = new Color32(255, 255, 255, 255);

                // vertices, uvs, normals
                {

                    for (int v = 0; v < numberOfCutsV; v++)
                    {

                        lerpValV = v / (float)(numberOfCutsV - 1);

                        thisMeter = Mathf.Lerp(thisStartMeterFrom, thisEndMeterTo, lerpValV);
                        otherMeter = Mathf.Lerp(otherStartMeterFrom, otherEndMeterTo, lerpValV);

                        pointFrom =
                            (useSampledPoints) ?
                            this.findSampledPointByMeter(thisMeter) :
                            this.findRawBezierPoint(thisMeter)
                            ;

                        pointTo =
                            (useSampledPoints) ?
                            otherBezier.findSampledPointByMeter(otherMeter) :
                            otherBezier.findRawBezierPoint(otherMeter)
                            ;

                        width = Vector3.Distance(pointFrom, pointTo);

                        uv.y = Mathf.Lerp(startUvY, endUvY, lerpValV);

                        startUvX = Mathf.Lerp(thisStartUvX, thisEndUvX, lerpValV);
                        endUvX = Mathf.Lerp(otherStartUvX, otherEndUvX, lerpValV);

                        uvCorrectionX =
                            (useUvPerspectiveCorrection && !Mathf.Approximately(startUvX, endUvX)) ?
                            width / (endUvX - startUvX) :
                            1.0f
                            ;

                        normalFrom = Vector3.Lerp(
                            this.startUpwards,
                            this.endUpwards,
                            (this.sampledTotalMeterLength > 0.0f) ? thisMeter / this.sampledTotalMeterLength : 0.0f)
                            .normalized
                            ;

                        normalTo = Vector3.Lerp(
                            otherBezier.startUpwards,
                            otherBezier.endUpwards,
                            (otherBezier.sampledTotalMeterLength > 0.0f) ? otherMeter / otherBezier.sampledTotalMeterLength : 0.0f)
                            .normalized
                            ;

                        vertexColor = Color32.Lerp(vertexColorFrom, vertexColorTo, lerpValV);

                        for (u = 0; u < numberOfCutsU; u++)
                        {

                            lerpValU = u / (float)(numberOfCutsU - 1);

                            index = u + (v * numberOfCutsU);

                            vertex =
                                (baseTransform) ?
                                baseTransform.InverseTransformPoint(Vector3.Lerp(pointFrom, pointTo, lerpValU)) :
                                Vector3.Lerp(pointFrom, pointTo, lerpValU)
                                ;

                            uvCorrectionY = (useUvPerspectiveCorrection && v > 0) ? Vector3.Distance(vertex, vertices[index - numberOfCutsU]) : 1.0f;

                            uv.x = Mathf.Lerp(startUvX, endUvX, lerpValU);

                            vertices[index] = vertex;

                            uvs[index] = new Vector4(uv.x * uvCorrectionX, uv.y * uvCorrectionY, uvCorrectionX, uvCorrectionY);
                            normals[index] = Vector3.Lerp(normalFrom, normalTo, lerpValU).normalized;
                            colors32[index] = vertexColor;

                        }

                    }

                }

                // triangles
                {

                    Vector3 dirA = (vertices[1] - vertices[0]).normalized;
                    Vector3 dirB = (vertices[numberOfCutsU] - vertices[0]).normalized;

                    bool flip = Vector3.Dot(upSideDirection, Vector3.Cross(dirA, dirB)) >= 0;

                    if (!flip)
                    {

                        for (int v = 0; v < numberOfCutsV - 1; v++)
                        {

                            for (u = 0; u < numberOfCutsU - 1; u++)
                            {

                                index = u + (v * numberOfCutsU);

                                triangles[trianglesCounter++] = index + 0;
                                triangles[trianglesCounter++] = index + numberOfCutsU;
                                triangles[trianglesCounter++] = index + 1;

                                triangles[trianglesCounter++] = index + 1;
                                triangles[trianglesCounter++] = index + numberOfCutsU;
                                triangles[trianglesCounter++] = index + numberOfCutsU + 1;

                            }

                        }

                    }

                    else
                    {

                        for (int v = 0; v < numberOfCutsV - 1; v++)
                        {

                            for (u = 0; u < numberOfCutsU - 1; u++)
                            {

                                index = u + (v * numberOfCutsU);

                                triangles[trianglesCounter++] = index + 0;
                                triangles[trianglesCounter++] = index + 1;
                                triangles[trianglesCounter++] = index + numberOfCutsU;

                                triangles[trianglesCounter++] = index + 1;
                                triangles[trianglesCounter++] = index + numberOfCutsU + 1;
                                triangles[trianglesCounter++] = index + numberOfCutsU;

                            }

                        }

                    }

                }

            }

            // set
            {

                dest.Clear();

                dest.vertices = vertices;
                dest.SetUVs(0, uvs);
                dest.triangles = triangles;
                dest.normals = normals;
                dest.colors32 = colors32;

                dest.RecalculateBounds();
                //dest.RecalculateNormals();

//#if UNITY_2018_1_OR_NEWER
//                dest.RecalculateTangents();
//#endif

                dest.UploadMeshData(false);

            }

        }

    }

}
