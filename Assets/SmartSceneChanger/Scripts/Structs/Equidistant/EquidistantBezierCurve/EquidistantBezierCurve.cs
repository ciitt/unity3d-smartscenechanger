﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Equidistant bezier curve
    /// </summary>
    [Serializable]
    public partial class EquidistantBezierCurve : EquidistantPointList
    {

        /// <summary>
        /// Point on bezier
        /// </summary>
        public class PointOnBezier
        {

            public Vector3 position = Vector3.zero;
            public Vector3 tangent = Vector3.forward;
            public Vector3 normalRight = Vector3.right;
            public Vector3 up = Vector3.up;

            public Quaternion calcQuaternion()
            {
                return Quaternion.LookRotation(this.tangent, this.up);
            }

        }

        /// <summary>
        /// Point at start
        /// </summary>
        public Vector3 p0 = Vector3.zero;

        /// <summary>
        /// Tangent point for p0
        /// </summary>
        public Vector3 p1 = Vector3.zero;

        /// <summary>
        /// Tangent point for p3
        /// </summary>
        public Vector3 p2 = Vector3.forward;

        /// <summary>
        /// Point at start
        /// </summary>
        public Vector3 p3 = Vector3.forward;

        /// <summary>
        /// Upwards at start point
        /// </summary>
        public Vector3 startUpwards = Vector3.up;

        /// <summary>
        /// Upwards at end point
        /// </summary>
        public Vector3 endUpwards = Vector3.up;

        /// <summary>
        /// Temp points
        /// </summary>
        protected static List<Vector3> TempPoints = new List<Vector3>();

        /// <summary>
        /// Temp PointOnBezier for any purposes
        /// </summary>
        protected static PointOnBezier TempPobForAny = new PointOnBezier();

        /// <summary>
        /// Temp value
        /// </summary>
        protected static float omt = 0f;

        /// <summary>
        /// Temp value
        /// </summary>
        protected static float omt2 = 0f;

        /// <summary>
        /// Temp value
        /// </summary>
        protected static float t2 = 0f;

        /// <summary>
        /// Constructor
        /// </summary>
        // ---------------------------------------------------------------------------------------
        public EquidistantBezierCurve() : base()
        {
            
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public EquidistantBezierCurve(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter
            ) : base()
        {
            this.update(_p0, _p1, _p2, _p3, _samplingUnitMeter);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        /// <param name="_startUpwards">start upwards</param>
        /// <param name="_endUpwards">end upwards</param>
        // ---------------------------------------------------------------------------------------
        public EquidistantBezierCurve(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter,
            Vector3 _startUpwards,
            Vector3 _endUpwards
            ) : base()
        {
            this.update(_p0, _p1, _p2, _p3, _samplingUnitMeter, _startUpwards, _endUpwards);
        }

        /// <summary>
        /// Update values
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        // ---------------------------------------------------------------------------------------
        public virtual void update(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter
            )
        {
            this.update(_p0, _p1, _p2, _p3, _samplingUnitMeter, Vector3.up, Vector3.up);
        }

        /// <summary>
        /// Update values
        /// </summary>
        /// <param name="_p0">point 0</param>
        /// <param name="_p1">point 1</param>
        /// <param name="_p2">point 2</param>
        /// <param name="_p3">point 3</param>
        /// <param name="_samplingUnitMeter">sampling unit meter</param>
        /// <param name="_startUpwards">start upwards</param>
        /// <param name="_endUpwards">end upwards</param>
        /// <returns>updated</returns>
        // ---------------------------------------------------------------------------------------
        public virtual void update(
            Vector3 _p0,
            Vector3 _p1,
            Vector3 _p2,
            Vector3 _p3,
            float _samplingUnitMeter,
            Vector3 _startUpwards,
            Vector3 _endUpwards
            )
        {

            // set
            {
                this.p0 = _p0;
                this.p1 = _p1;
                this.p2 = _p2;
                this.p3 = _p3;
                this.startUpwards = _startUpwards;
                this.endUpwards = _endUpwards;
            }

            // ---------------------

            // tempPoints
            {

                // clear tempPoints
                {
                    TempPoints.Clear();
                }

                // set
                {

                    float distance =
                        Vector3.Distance(this.p0, this.p1) +
                        Vector3.Distance(this.p1, this.p2) +
                        Vector3.Distance(this.p2, this.p3)
                        ;

                    int sampling =
                        (_samplingUnitMeter > 0.0f) ?
                        Mathf.Max(2, Mathf.RoundToInt(distance / _samplingUnitMeter)) :
                        2
                        ;

                    float sampling_t01 = 0.0f;

                    for (int i = 0; i < sampling; i++)
                    {

                        sampling_t01 = i / (float)(sampling - 1);

                        TempPoints.Add(this.findRawBezierPoint(sampling_t01));

                    }

                }

            }

            // base.update
            {
                base.update(TempPoints, _samplingUnitMeter);
            }

            // clear tempPoints
            {
                TempPoints.Clear();
            }

        }

        /// <summary>
        /// Find a detail info on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void findRawBezierPoint(float t01, PointOnBezier dest)
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            dest.position = this.findRawBezierPoint(t01);
            dest.tangent = this.findRawBezierTangent(t01);
            dest.normalRight = this.findRawBezierRightNormal(dest.tangent, t01);
            dest.up = this.findRawBezierUpVector(dest.tangent, dest.normalRight);

        }

        /// <summary>
        /// Find a sampled detail info on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void findSampledBezierPointByNormalizedValue(float t01, PointOnBezier dest)
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            dest.position = this.findSampledPointByNormalizeValue(t01);
            dest.tangent = this.findRawBezierTangent(t01);

            t01 = this.smoothLerpVal(t01);

            dest.normalRight = this.findRawBezierRightNormal(dest.tangent, t01);
            dest.up = this.findRawBezierUpVector(dest.tangent, dest.normalRight);

        }

        /// <summary>
        /// Find a sampled detail info on a bezier curve
        /// </summary>
        /// <param name="targetMeter">target meter</param>
        /// <param name="dest">destination</param>
        // -------------------------------------------------------------------------------------------
        public virtual void findSampledBezierPointByMeter(float targetMeter, PointOnBezier dest)
        {

            if (dest == null)
            {
                return;
            }

            // ---------------------

            float t01 =
                (this.sampledTotalMeterLength > 0) ?
                Mathf.Clamp01(targetMeter / this.sampledTotalMeterLength) :
                0.0f
                ;

            this.findSampledBezierPointByNormalizedValue(t01, dest);

        }

        /// <summary>
        /// Smooth lerp value
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>smoothed value</returns>
        // -------------------------------------------------------------------------------------------
        protected virtual float smoothLerpVal(float t01)
        {
            return Mathf.SmoothStep(0.0f, 1.0f, t01);
        }

        /// <summary>
        /// Convert sampled meter to normalized value
        /// </summary>
        /// <param name="meter">meter position</param>
        /// <returns>normalized value</returns>
        // -------------------------------------------------------------------------------------------
        public virtual float convertSampledMeterToNormalized01(float meter)
        {
            return
                (this.sampledTotalMeterLength > 0) ?
                meter / this.sampledTotalMeterLength :
                0
                ;
        }

        /// <summary>
        /// Find a point on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>point</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 findRawBezierPoint(float t01)
        {

            // t01 = Mathf.Clamp01(t01);

            omt = 1.0f - t01;
            omt2 = omt * omt;
            t2 = t01 * t01;

            return
                (this.p0 * (omt2 * omt)) +
                (this.p1 * (3f * omt2 * t01)) +
                (this.p2 * (3f * omt * t2)) +
                (this.p3 * (t2 * t01))
                ;

        }

        /// <summary>
        /// Find a tangent on a bezier curve
        /// </summary>
        /// <param name="t01">normalized t</param>
        /// <returns>tangent</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 findRawBezierTangent(float t01)
        {

            // t01 = Mathf.Clamp01(t01);

            omt = 1.0f - t01;
            omt2 = omt * omt;
            t2 = t01 * t01;

            return
                (
                (this.p0 * (-omt2)) +
                (this.p1 * (3 * omt2 - 2 * omt)) +
                (this.p2 * (-3 * t2 + 2 * t01)) +
                (this.p3 * (t2))
                )
                .normalized
                ;

        }

        /// <summary>
        /// Find a right normal on a bezier curve
        /// </summary>
        /// <param name="tangent">tangent</param>
        /// <param name="t01">normalized t</param>
        /// <returns>right normal</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 findRawBezierRightNormal(Vector3 tangent, float t01)
        {
            
            if (tangent.sqrMagnitude <= 0.0f)
            {
                return Vector3.zero;
            }

            return Quaternion.LookRotation(tangent, Vector3.Lerp(this.startUpwards, this.endUpwards, t01)) * Vector3.right;

        }

        /// <summary>
        /// Find a up vector on a bezier curve
        /// </summary>
        /// <param name="tangent">tangent</param>
        /// <param name="normalRight">normalRight</param>
        /// <returns>up vector</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector3 findRawBezierUpVector(Vector3 tangent, Vector3 normalRight)
        {
            return Vector3.Cross(tangent, normalRight);
        }

        /// <summary>
        /// Calculate uv max position from width
        /// </summary>
        /// <param name="width">width</param>
        /// <returns>uv max position</returns>
        // -------------------------------------------------------------------------------------------
        public virtual Vector2 calcBezierUvMaxFromWidth(float width)
        {

            if (width <= 0.0f)
            {
                return Vector2.zero;
            }

            return new Vector2(1, Mathf.Max(1, Mathf.RoundToInt(this.sampledTotalMeterLength / width)));

        }

        /// <summary>
        /// Find sampled points
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="numberOfCuts">number of cuts (val > 1)</param>
        /// <param name="startOffset">start offset</param>
        /// <param name="endOffset">end offset</param>
        // -------------------------------------------------------------------------------------------
        public virtual void findSampledPoints(List<Vector3> dest, int numberOfCuts, Vector3 startOffset, Vector3 endOffset)
        {

            if (numberOfCuts <= 1)
            {
                return;
            }

            // -------------------

            dest.Clear();

            float lerpVal = 0.0f;

            Vector3 pos = Vector3.zero;
            Vector3 offset = Vector3.zero;

            // -------------------

            for (int i = 0; i < numberOfCuts; i++)
            {

                lerpVal = i / (float)(numberOfCuts - 1);

                this.findSampledBezierPointByNormalizedValue(lerpVal, TempPobForAny);

                offset = Vector3.Lerp(startOffset, endOffset, lerpVal);

                pos = TempPobForAny.position;
                pos += TempPobForAny.normalRight * offset.x;
                pos += TempPobForAny.up * offset.y;
                pos += TempPobForAny.tangent * offset.z;

                dest.Add(pos);

            }

        }

    }

}

