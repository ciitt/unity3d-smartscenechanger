﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Interface for ConsecutiveEquidistantBezierCurves
    /// </summary>
    public interface IConsecutiveEquidistantBezierCurves
    {

        ConsecutiveEquidistantBezierCurves cBezier
        {
            get;
            set;
        }

    }

}
