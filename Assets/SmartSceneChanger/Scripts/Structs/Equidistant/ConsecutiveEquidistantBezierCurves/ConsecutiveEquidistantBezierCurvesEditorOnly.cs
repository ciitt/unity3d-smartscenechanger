﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Consecutive EquidistantBezierCurve
    /// </summary>
    public partial class ConsecutiveEquidistantBezierCurves
    {

        /// <summary>
        /// Draw debug bezier line (EditorOnly)
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierLineEditorOnly()
        {

            foreach (var val in this.bezieList)
            {
                val.drawDebugBezierLineEditorOnly();
            }

        }

        /// <summary>
        /// Draw debug bezier line (EditorOnly)
        /// </summary>
        /// <param name="color">line color</param>
        /// <param name="width">line width</param>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierLineEditorOnly(Color color, float width)
        {

            foreach (var val in this.bezieList)
            {
                val.drawDebugBezierLineEditorOnly(color, width);
            }

        }

        /// <summary>
        /// Draw debug bezier point lines (EditorOnly)
        /// </summary>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierPointLinesEditorOnly()
        {

            foreach (var val in this.bezieList)
            {
                val.drawDebugBezierPointLinesEditorOnly();
            }

        }

        /// <summary>
        /// Draw debug bezier point lines (EditorOnly)
        /// </summary>
        /// <param name="color">line color</param>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierPointLinesEditorOnly(Color color)
        {

            foreach (var val in this.bezieList)
            {
                val.drawDebugBezierPointLinesEditorOnly(color);
            }

        }

        /// <summary>
        /// Draw debug bezier position info (EditorOnly)
        /// </summary>
        /// <param name="position01">normalized position</param>
        // ------------------------------------------------------------------------------------------
        public void drawDebugBezierPositionInfoEditorOnly(float position01)
        {

            float t01 = 0.0f;
            EquidistantBezierCurve bezier = null;

            if (this.findTargetBezier(this.sampledAllTotalMeterLength * position01, ref bezier, ref t01))
            {
                bezier.drawDebugBezierPositionInfoEditorOnly(t01);
            }

        }

    }

}

#endif
