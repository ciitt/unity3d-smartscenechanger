﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// EquidistantBezierCurve script
    /// </summary>
    [ExecuteInEditMode]
    public class EquidistantBezierScript : MonoBehaviour, IEquidistantBezierCurve
    {

        /// <summary>
        /// Point 0
        /// </summary>
        [SerializeField]
        [Tooltip("Point 0")]
        Transform m_p0 = null;

        /// <summary>
        /// Point 1
        /// </summary>
        [SerializeField]
        [Tooltip("Point 1")]
        Transform m_p1 = null;

        /// <summary>
        /// Point 2
        /// </summary>
        [SerializeField]
        [Tooltip("Point 2")]
        Transform m_p2 = null;

        /// <summary>
        /// Point 3
        /// </summary>
        [SerializeField]
        [Tooltip("Point 3")]
        Transform m_p3 = null;

        /// <summary>
        /// Sampling unit meter
        /// </summary>
        [SerializeField]
        [Tooltip("Sampling unit meter")]
        [Range(0.1f, 10.0f)]
        float m_samplingUnitMeter = 1.0f;

        [Space(30.0f), Header("Road Mesh")]

        /// <summary>
        /// Reference to MeshFilter for a bezier road mesh
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to MeshFilter for a bezier road mesh")]
        MeshFilter m_refMeshFilter = null;

        /// <summary>
        /// Road width
        /// </summary>
        [SerializeField]
        [Tooltip("Road width")]
        [Range(0.1f, 10.0f)]
        float m_roadWidth = 1.0f;

        /// <summary>
        /// Number of cuts U for road
        /// </summary>
        [SerializeField]
        [Tooltip("Number of cuts U for road")]
        [Range(2, 10)]
        int m_numberOfCutsU = 2;

        /// <summary>
        /// Repeat meter value for uv V
        /// </summary>
        [SerializeField]
        [Tooltip("Repeat meter value for uv V")]
        [Range(0.1f, 10.0f)]
        float m_repeatUvMeterV = 1.0f;

        /// <summary>
        /// Use uv perspective correction
        /// </summary>
        [SerializeField]
        [Tooltip("Use uv perspective correction")]
        bool m_useUvPerspectiveCorrection = true;




        /// <summary>
        /// EquidistantBezierCurve
        /// </summary>
        [HideInInspector]
        [SerializeField]
        EquidistantBezierCurve m_bezier = new EquidistantBezierCurve();

        /// <summary>
        /// Road mesh
        /// </summary>
        [HideInInspector]
        [SerializeField]
        Mesh m_mesh = null;




#if UNITY_EDITOR

        [Space(30.0f), Header("EditoOnly")]

        /// <summary>
        /// Target to align (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Target to align (EditorOnly)")]
        Transform m_refAlignTargetEditorOnly = null;

        /// <summary>
        /// Position for m_refAlignTarget (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Position for m_refAlignTarget (EditorOnly)")]
        [Range(0.0f, 1.0f)]
        float m_alignTargetPositionEditorOnly = 0.0f;

        /// <summary>
        /// Draw gizmo lines (EditorOnly)
        /// </summary>
        [Space(30.0f)]
        [SerializeField]
        [Tooltip("Draw gizmo lines (EditorOnly)")]
        bool m_drawGizmoLinesEditorOnly = true;

        /// <summary>
        /// Temp PointOnBezier (EditorOnly)
        /// </summary>
        EquidistantBezierCurve.PointOnBezier m_pobEditorOnly = new EquidistantBezierCurve.PointOnBezier();

#endif

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// EquidistantBezierCurve
        /// </summary>
        public EquidistantBezierCurve bezier
        {
            get { return this.m_bezier; }
            set { this.m_bezier = value; }
        }

        /// <summary>
        /// Point 0
        /// </summary>
        public Transform point0
        {
            get { return this.m_p0; }
            set { this.m_p0 = value; }
        }

        /// <summary>
        /// Point 1
        /// </summary>
        public Transform point1
        {
            get { return this.m_p1; }
            set { this.m_p1 = value; }
        }

        /// <summary>
        /// Point 2
        /// </summary>
        public Transform point2
        {
            get { return this.m_p2; }
            set { this.m_p2 = value; }
        }

        /// <summary>
        /// Point 3
        /// </summary>
        public Transform point3
        {
            get { return this.m_p3; }
            set { this.m_p3 = value; }
        }

        /// <summary>
        /// Awake
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void Awake()
        {

            this.updateBezier();

        }

#if UNITY_EDITOR

        /// <summary>
        /// Update
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void Update()
        {

            if (Application.isPlaying)
            {
                return;
            }

            // ------------------

            //
            {
                this.updateBezier();
                this.updateMesh();
            }

            //
            {
                this.alignTargetObjectEditorOnly();
            }

        }

#endif

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void OnDestroy()
        {

            if (Application.isPlaying)
            {
                Destroy(this.m_mesh);
            }

            else
            {
                DestroyImmediate(this.m_mesh);
            }

        }

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void OnDrawGizmos()
        {

#if UNITY_EDITOR

            if (UnityEditor.EditorApplication.isCompiling || !this.m_drawGizmoLinesEditorOnly)
            {
                return;
            }

            this.m_bezier.drawDebugBezierLineEditorOnly();
            this.m_bezier.drawDebugBezierPointLinesEditorOnly();
            this.m_bezier.drawDebugBezierPositionInfoEditorOnly(this.m_alignTargetPositionEditorOnly);

#endif

        }


#if UNITY_EDITOR

        /// <summary>
        /// Create a new single bezier EditorOnly
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public static void createNewSingleBezierCurveEditorOnly()
        {

            GameObject obj = new GameObject(UnityEditor.GameObjectUtility.GetUniqueNameForSibling(null, "Equidistant Bezier Curve (1)"));

            EquidistantBezierScript bezier = obj.AddComponent<EquidistantBezierScript>();

            // ----------------------

            // points
            {

                GameObject point0 = new GameObject("Point (0)");
                GameObject point1 = new GameObject("Point (1)");
                GameObject point2 = new GameObject("Point (2)");
                GameObject point3 = new GameObject("Point (3)");

                // SetParent
                {
                    point0.transform.SetParent(obj.transform);
                    point1.transform.SetParent(obj.transform);
                    point2.transform.SetParent(obj.transform);
                    point3.transform.SetParent(obj.transform);
                }

                // position
                {
                    point0.transform.position = Vector3.zero;
                    point1.transform.position = new Vector3(0, 0, 5.0f);
                    point2.transform.position = new Vector3(5.0f, 0, 5.0f);
                    point3.transform.position = new Vector3(5.0f, 0, 0);
                }

                // set
                {
                    bezier.point0 = point0.transform;
                    bezier.point1 = point1.transform;
                    bezier.point2 = point2.transform;
                    bezier.point3 = point3.transform;
                }

            }

        }

#endif

        /// <summary>
        /// Update bezier
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void updateBezier()
        {

            if (
                !this.m_p0 ||
                !this.m_p1 ||
                !this.m_p2 ||
                !this.m_p3
                )
            {
                return;
            }

            // ----------------------------------

            this.m_bezier.update(
                this.m_p0.position,
                this.m_p1.position,
                this.m_p2.position,
                this.m_p3.position,
                this.m_samplingUnitMeter,
                this.m_p0.up,
                this.m_p3.up
                );

        }

        /// <summary>
        /// Update mesh
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void updateMesh()
        {

            if (this.m_refMeshFilter)
            {

                if (!this.m_mesh)
                {
                    this.m_mesh = new Mesh();
                    this.m_mesh.name = this.name;
                }

                this.m_refMeshFilter.sharedMesh = this.m_mesh;

                this.m_bezier.createSimpleRoad(
                    this.m_roadWidth,
                    this.m_roadWidth,
                    this.m_numberOfCutsU,
                    this.m_repeatUvMeterV,
                    this.m_useUvPerspectiveCorrection,
                    this.m_mesh,
                    this.m_refMeshFilter.transform
                    );

            }

            else
            {

                if (this.m_mesh)
                {
                    this.OnDestroy();
                }

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// Align a target object (EditorOnly)
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void alignTargetObjectEditorOnly()
        {

            if (!this.m_refAlignTargetEditorOnly)
            {
                return;
            }

            // --------------------------

            // findSampledBezierPointByNormalizedValue
            {
                this.m_bezier.findSampledBezierPointByNormalizedValue(this.m_alignTargetPositionEditorOnly, this.m_pobEditorOnly);
            }

            //
            {
                this.m_refAlignTargetEditorOnly.position = this.m_pobEditorOnly.position;
                this.m_refAlignTargetEditorOnly.rotation = this.m_pobEditorOnly.calcQuaternion();
            }

        }

#endif

    }

}
