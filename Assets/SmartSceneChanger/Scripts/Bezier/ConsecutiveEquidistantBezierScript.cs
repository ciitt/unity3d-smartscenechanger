﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// ConsecutiveEquidistantBezierCurves script
    /// </summary>
    [ExecuteInEditMode]
    public class ConsecutiveEquidistantBezierScript : MonoBehaviour, IConsecutiveEquidistantBezierCurves
    {

        /// <summary>
        /// Close bezier curve
        /// </summary>
        [SerializeField]
        [Tooltip("Close bezier curve")]
        bool close = true;

        /// <summary>
        /// Tangent scale
        /// </summary>
        [SerializeField]
        [Tooltip("Tangent scale")]
        [Range(0.0f, 10.0f)]
        float tangentScale = 1.0f;

        /// <summary>
        /// Sampling unit meter
        /// </summary>
        [SerializeField]
        [Tooltip("Sampling unit meter")]
        [Range(0.1f, 10.0f)]
        float m_samplingUnitMeter = 1.0f;

        /// <summary>
        /// Point list
        /// </summary>
        [SerializeField]
        [Tooltip("Point list")]
        List<Transform> m_points = new List<Transform>();

        [Space(30.0f), Header("Road Mesh")]

        /// <summary>
        /// Reference to MeshFilter for a bezier road mesh
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to MeshFilter for a bezier road mesh")]
        MeshFilter m_refMeshFilter = null;

        /// <summary>
        /// Road width
        /// </summary>
        [SerializeField]
        [Tooltip("Road width")]
        [Range(0.1f, 10.0f)]
        float m_roadWidth = 1.0f;

        /// <summary>
        /// Number of cuts U for road
        /// </summary>
        [SerializeField]
        [Tooltip("Number of cuts U for road")]
        [Range(2, 10)]
        int m_numberOfCutsU = 2;

        /// <summary>
        /// Repeat meter value for uv V
        /// </summary>
        [SerializeField]
        [Tooltip("Repeat meter value for uv V")]
        [Range(0.1f, 10.0f)]
        float m_repeatUvMeterV = 1.0f;

        /// <summary>
        /// Use uv perspective correction
        /// </summary>
        [SerializeField]
        [Tooltip("Use uv perspective correction")]
        bool m_useUvPerspectiveCorrection = true;

        /// <summary>
        /// ConsecutiveEquidistantBezierCurves
        /// </summary>
        ConsecutiveEquidistantBezierCurves m_cBezier = new ConsecutiveEquidistantBezierCurves();

        /// <summary>
        /// Road mesh
        /// </summary>
        [HideInInspector]
        [SerializeField]
        Mesh m_mesh = null;

        /// <summary>
        /// Mesh instances
        /// </summary>
        List<Mesh> m_meshInstances = new List<Mesh>();

#if UNITY_EDITOR

        [Space(30.0f), Header("EditoOnly")]

        /// <summary>
        /// Target to align (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Target to align (EditorOnly)")]
        Transform m_refAlignTargetEditorOnly = null;

        /// <summary>
        /// Position for m_refAlignTarget (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Position for m_refAlignTarget (EditorOnly)")]
        [Range(0.0f, 1.0f)]
        float m_alignTargetPositionEditorOnly = 0.0f;

        /// <summary>
        /// Draw gizmo lines (EditorOnly)
        /// </summary>
        [SerializeField]
        [Tooltip("Draw gizmo lines (EditorOnly)")]
        bool m_drawGizmoLinesEditorOnly = true;

        /// <summary>
        /// Temp PointOnBezier (EditorOnly)
        /// </summary>
        EquidistantBezierCurve.PointOnBezier m_pobEditorOnly = new EquidistantBezierCurve.PointOnBezier();

#endif

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// ConsecutiveEquidistantBezierCurves
        /// </summary>
        public ConsecutiveEquidistantBezierCurves cBezier
        {
            get { return this.m_cBezier; }
            set { this.m_cBezier = value; }
        }

        /// <summary>
        /// Point list
        /// </summary>
        public List<Transform> points
        {
            get { return this.m_points; }
            set { this.m_points = value; }
        }

        /// <summary>
        /// Awake
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void Awake()
        {

            this.updateBezier();

        }

#if UNITY_EDITOR

        /// <summary>
        /// Update
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void Update()
        {

            if (Application.isPlaying)
            {
                return;
            }

            // ------------------

            //
            {
                this.updateBezier();
                this.updateMesh();
            }

            //
            {
                this.alignTargetObjectEditorOnly();
            }

        }

#endif

        /// <summary>
        /// OnDestroy
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void OnDestroy()
        {

            if (Application.isPlaying)
            {
                Destroy(this.m_mesh);
            }

            else
            {
                DestroyImmediate(this.m_mesh);
            }

        }

        /// <summary>
        /// OnDrawGizmos
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        private void OnDrawGizmos()
        {

#if UNITY_EDITOR

            if (UnityEditor.EditorApplication.isCompiling || !this.m_drawGizmoLinesEditorOnly)
            {
                return;
            }

            this.m_cBezier.drawDebugBezierLineEditorOnly();
            this.m_cBezier.drawDebugBezierPointLinesEditorOnly();
            this.m_cBezier.drawDebugBezierPositionInfoEditorOnly(this.m_alignTargetPositionEditorOnly);

#endif

        }


#if UNITY_EDITOR

        /// <summary>
        /// Create a new consecutive bezier EditorOnly
        /// </summary>
        // -------------------------------------------------------------------------------------------
        public static void createNewConsecutiveBezierCurveEditorOnly()
        {

            GameObject obj = new GameObject(UnityEditor.GameObjectUtility.GetUniqueNameForSibling(null, "Consecutive Equidistant Bezier Curve (1)"));

            ConsecutiveEquidistantBezierScript bezier = obj.AddComponent<ConsecutiveEquidistantBezierScript>();

            // ----------------------

            // points
            {

                GameObject point0 = new GameObject("Point (0)");
                GameObject point1 = new GameObject("Point (1)");
                GameObject point2 = new GameObject("Point (2)");
                GameObject point3 = new GameObject("Point (3)");

                // SetParent
                {
                    point0.transform.SetParent(obj.transform);
                    point1.transform.SetParent(obj.transform);
                    point2.transform.SetParent(obj.transform);
                    point3.transform.SetParent(obj.transform);
                }

                // position
                {
                    point0.transform.position = Vector3.zero;
                    point1.transform.position = new Vector3(0, 0, 5.0f);
                    point2.transform.position = new Vector3(5.0f, 0, 5.0f);
                    point3.transform.position = new Vector3(5.0f, 0, 0);
                }

                // set
                {
                    bezier.points.Add(point0.transform);
                    bezier.points.Add(point1.transform);
                    bezier.points.Add(point2.transform);
                    bezier.points.Add(point3.transform);
                }

            }

        }

#endif

        /// <summary>
        /// Update bezier
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void updateBezier()
        {

            this.m_cBezier.SetPointList(this.m_points, this.m_samplingUnitMeter, this.close, this.tangentScale);

        }

        /// <summary>
        /// Update mesh
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void updateMesh()
        {

            if (this.m_refMeshFilter)
            {

                if (!this.m_mesh)
                {
                    this.m_mesh = new Mesh();
                    this.m_mesh.name = this.name;
                }

                //
                {
                    this.m_refMeshFilter.sharedMesh = this.m_mesh;
                }

                // 
                {

                    int size = this.m_cBezier.bezieList.Count;

                    // m_meshInstances
                    {

                        for (int i = this.m_meshInstances.Count - 1; i >= size; i--)
                        {
                            DestroyImmediate(this.m_meshInstances[i]);
                            this.m_meshInstances.RemoveAt(i);
                        }

                        int add = size - this.m_meshInstances.Count;

                        for (int i = 0; i < add; i++)
                        {
                            this.m_meshInstances.Add(new Mesh());
                        }

                    }

                    // createSimpleRoad
                    {

                        for (int i = 0; i < size; i++)
                        {

                            this.m_cBezier.bezieList[i].createSimpleRoad(
                                this.m_roadWidth,
                                this.m_roadWidth,
                                this.m_numberOfCutsU,
                                this.m_repeatUvMeterV,
                                this.m_useUvPerspectiveCorrection,
                                this.m_meshInstances[i],
                                this.m_refMeshFilter.transform
                                );

                        }

                    }

                    // CombineMeshes
                    {
                        Funcs.CombineMeshes(this.m_mesh, this.m_meshInstances);
                    }

                }

            }

            else
            {

                if (this.m_mesh)
                {
                    this.OnDestroy();
                }

            }

        }

#if UNITY_EDITOR

        /// <summary>
        /// Align a target object (EditorOnly)
        /// </summary>
        // ----------------------------------------------------------------------------------------------
        void alignTargetObjectEditorOnly()
        {

            if (!this.m_refAlignTargetEditorOnly)
            {
                return;
            }

            // --------------------------

            // findSampledBezierPointByNormalizedValue
            {
                this.m_cBezier.findSampledBezierPointByNormalizedValue(this.m_alignTargetPositionEditorOnly, this.m_pobEditorOnly);
            }

            //
            {
                this.m_refAlignTargetEditorOnly.position = this.m_pobEditorOnly.position;
                this.m_refAlignTargetEditorOnly.rotation = this.m_pobEditorOnly.calcQuaternion();
            }

        }

#endif

    }

}
