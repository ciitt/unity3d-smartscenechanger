﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEditor;

namespace SSC
{

    /// <summary>
    /// Static Functions for EditorOnly
    /// </summary>
    public static class FuncsEditorOnly
    {

        /// <summary>
        /// Ply line width (EditorOnly)
        /// </summary>
        public static float PolyLineWidth = 10.0f;

        /// <summary>
        /// Check duplicates (EditorOnly)
        /// </summary>
        /// <typeparam name="T1">IEnumerable</typeparam>
        /// <typeparam name="T2">key</typeparam>
        /// <param name="source">source</param>
        /// <param name="keySelector">keySelector</param>
        /// <param name="transform">Transform</param>
        // ------------------------------------------------------------------------------------------------------------------------------
        public static void checkDuplicates<T1, T2>(IEnumerable<T1> source, System.Func<T1,T2> keySelector, Transform transform)
        {

            var temp = source.GroupBy(keySelector).Where(val => val.Count() > 1);
            
            foreach(var val in temp)
            {
                Debug.LogWarningFormat(
                    "(#if UNITY_EDITOR) : Found duplicates : {0} : {1}",
                    val.Key,
                    Funcs.CreateHierarchyPath(transform)
                    );
            }

        }

        /// <summary>
        /// Checl if the file path starts with Application.dataPath
        /// </summary>
        /// <param name="filePath">file path</param>
        /// <param name="showDialogIfError">show dialog if false</param>
        /// <returns>yes</returns>
        // ------------------------------------------------------------------------------------------------------------------------------
        public static bool checkFilePathStartsWithDataPath(string filePath, bool showDialogIfFalse)
        {

            bool ret = filePath.StartsWith(Application.dataPath, StringComparison.Ordinal);

            if (showDialogIfFalse && !ret)
            {
                EditorUtility.DisplayDialog("Error", "Out of Assets folder", "OK");
            }

            return ret;

        }

    }

}

#endif
