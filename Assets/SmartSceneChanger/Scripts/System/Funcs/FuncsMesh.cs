﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Static Functions for Mesh
    /// </summary>
    public partial class Funcs
    {

        /// <summary>
        /// Combine meshes
        /// </summary>
        /// <param name="dest">destination</param>
        /// <param name="meshChunkList">mesh chunk list</param>
        /// <param name="recalculateNormals">use RecalculateNormals</param>
        /// <param name="markNoLogerReadable">flag for UploadMeshData</param>
        /// <param name="forMeshCollider">use for MeshCollider</param>
        /// <param name="recalculateTangents">use RecalculateTangents</param>
        // -------------------------------------------------------------------------------------------
        public static void CombineMeshes(
            Mesh dest,
            IList<Mesh> meshChunkList,
            bool recalculateNormals = true,
            bool markNoLogerReadable = false,
            bool forMeshCollider = false,
            bool recalculateTangents = true
            )
        {

            int size = meshChunkList.Count;

            // ------------------

            if (!dest)
            {
                return;
            }

            if (size <= 0)
            {
                dest.Clear();
                return;
            }

            // ------------------

            int validSize = 0;

            // ------------------

            for (int i = 0; i < size; i++)
            {

                if (meshChunkList[i] && meshChunkList[i].vertexCount >= 3)
                {
                    validSize++;
                }

            }

            // ------------------

            if (validSize <= 0)
            {
                dest.Clear();
                return;
            }

            // ------------------

            CombineInstance[] combine = new CombineInstance[validSize];

            // ------------------

            //
            {

                int validCounter = 0;

                for (int i = 0; i < size; i++)
                {

                    if (!meshChunkList[i] || meshChunkList[i].vertexCount < 3)
                    {
                        continue;
                    }

                    // set
                    {
                        combine[validCounter].mesh = meshChunkList[i];
                        combine[validCounter].transform = Matrix4x4.identity;
                        validCounter++;
                    }

                }

            }

            // CombineMeshes
            {

                dest.Clear();
                dest.CombineMeshes(combine);

                dest.RecalculateBounds();

                if (forMeshCollider)
                {
                    dest.uv = null;
                    dest.uv2 = null;
                    dest.uv3 = null;
                    dest.uv4 = null;
                    dest.uv5 = null;
                    dest.uv6 = null;
                    dest.uv7 = null;
                    dest.uv8 = null;
                    dest.normals = null;
                    dest.colors32 = null;
                }

                else
                {

                    if (recalculateNormals)
                    {
                        dest.RecalculateNormals();
                    }

#if UNITY_2018_1_OR_NEWER

                    if (recalculateTangents)
                    {
                        dest.RecalculateTangents();
                    }

#endif

                }

                dest.UploadMeshData(markNoLogerReadable);

            }

        }

        /// <summary>
        /// Copy mesh along Z
        /// </summary>
        /// <param name="chunkOriginMesh">chunk origin mesh</param>
        /// <param name="dest">destination</param>
        /// <param name="targetMeterLength">target meter length</param>
        /// <param name="spacingMeter">spacing meter</param>
        /// <param name="moveUvY">move uv.y</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CopyMeshAlongZ(
            Mesh chunkOriginMesh,
            Mesh dest,
            float targetMeterLength,
            float spacingMeter,
            bool moveUvY
            )
        {

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (chunkOriginMesh && !chunkOriginMesh.isReadable)
            {
                Debug.LogWarning("The mesh is not readable : " + chunkOriginMesh.name);
            }

#endif

            if (
                !chunkOriginMesh ||
                !chunkOriginMesh.isReadable ||
                targetMeterLength <= 0.0f ||
                !dest ||
                spacingMeter < 0.0f
                )
            {
                return false;
            }

            // ------------------------

            Bounds bounds = chunkOriginMesh.bounds;

            float lengthZ = bounds.size.z + spacingMeter;

            // ------------------------

            if (lengthZ <= 0.0f)
            {
                return false;
            }

            // ------------------------

            int num = Mathf.Max(1, Mathf.RoundToInt(targetMeterLength / lengthZ));

            CombineInstance[] combine = new CombineInstance[num];

            int shiftUv = 0;

            // ------------------------

            // clear
            {
                dest.Clear();
            }

            // 0
            {
                combine[0].mesh = UnityEngine.Object.Instantiate(chunkOriginMesh);
                combine[0].transform = Matrix4x4.identity;
            }

            // 1 -> N
            {

                Vector2[] uvs = null;

                Mesh tempMesh = null;

                int j = 0;

                for (int i = 1; i < num; i++)
                {

                    // tempMesh
                    {
                        tempMesh = UnityEngine.Object.Instantiate(chunkOriginMesh);
                    }

                    // uv
                    {

                        if (moveUvY)
                        {

                            shiftUv++;

                            uvs = tempMesh.uv;

                            for (j = uvs.Length - 1; j >= 0; j--)
                            {
                                uvs[j].y -= shiftUv;
                            }

                            tempMesh.uv = uvs;

                        }

                    }

                    // set
                    {
                        combine[i].mesh = tempMesh;
                        combine[i].transform = Matrix4x4.TRS(Vector3.forward * lengthZ * i, Quaternion.identity, Vector3.one);
                    }

                }

            }

            // CombineMeshes
            {
                dest.CombineMeshes(combine);
            }

            // destroy
            {

                if (Application.isPlaying)
                {
                    foreach (var val in combine)
                    {
                        UnityEngine.Object.Destroy(val.mesh);
                    }
                }

                else
                {
                    foreach (var val in combine)
                    {
                        UnityEngine.Object.DestroyImmediate(val.mesh);
                    }
                }

            }

            return true;

        }

        /// <summary>
        /// Copy mesh along Z
        /// </summary>
        /// <param name="chunkOriginMeshAtStart">chunk mesh at start</param>
        /// <param name="chunkOriginMeshMainLoop">chunk mesh for main loop</param>
        /// <param name="chunkOriginMeshAtEnd">chunk mesh at end</param>
        /// <param name="dest">destination</param>
        /// <param name="targetMeterLength">target meter length</param>
        /// <param name="spacingMeter">spacing meter</param>
        /// <param name="moveUvY">move uv.y</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool CopyMeshAlongZ(
            Mesh chunkOriginMeshAtStart,
            Mesh chunkOriginMeshMainLoop,
            Mesh chunkOriginMeshAtEnd,
            Mesh dest,
            float targetMeterLength,
            float spacingMeter,
            bool moveUvY
            )
        {

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (chunkOriginMeshAtStart && !chunkOriginMeshAtStart.isReadable)
            {
                Debug.LogWarning("The mesh is not readable : " + chunkOriginMeshAtStart.name);
            }

            if (chunkOriginMeshMainLoop && !chunkOriginMeshMainLoop.isReadable)
            {
                Debug.LogWarning("The mesh is not readable : " + chunkOriginMeshMainLoop.name);
            }

            if (chunkOriginMeshAtEnd && !chunkOriginMeshAtEnd.isReadable)
            {
                Debug.LogWarning("The mesh is not readable : " + chunkOriginMeshAtEnd.name);
            }

#endif

            if (
                !chunkOriginMeshAtStart ||
                !chunkOriginMeshAtStart.isReadable ||
                !chunkOriginMeshMainLoop ||
                !chunkOriginMeshMainLoop.isReadable ||
                !chunkOriginMeshAtEnd ||
                !chunkOriginMeshAtEnd.isReadable ||
                targetMeterLength <= 0.0f ||
                !dest ||
                spacingMeter < 0.0f
                )
            {
                return false;
            }

            // ------------------------

            float boundsAtStartZ = chunkOriginMeshAtStart.bounds.size.z;
            float boundsForMainZ = chunkOriginMeshMainLoop.bounds.size.z;
            float boundsAtEndZ = chunkOriginMeshAtEnd.bounds.size.z;

            // ------------------------

            if (boundsAtStartZ + boundsForMainZ + boundsAtEndZ + spacingMeter <= 0.0f)
            {
                return false;
            }

            // ------------------------

            int mainLoopNum = Mathf.Max(1, Mathf.RoundToInt((targetMeterLength - boundsAtStartZ - boundsAtEndZ) / (boundsForMainZ + spacingMeter)));

            CombineInstance[] combine = new CombineInstance[mainLoopNum + 2];

            float pos = 0.0f;

            int combineSize = combine.Length;

            int shiftUv = 0;

            // ------------------------

            // clear
            {
                dest.Clear();
            }

            // 0
            {
                combine[0].mesh = UnityEngine.Object.Instantiate(chunkOriginMeshAtStart);
                combine[0].transform = Matrix4x4.identity;
                //combine[0].transform = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0, 180, 0), Vector3.one);
            }

            // 1 -> N
            {

                Vector2[] uvs = null;

                Mesh tempMesh = null;

                int j = 0;

                for (int i = 1; i < combineSize - 1; i++)
                {

                    // tempMesh
                    {
                        tempMesh = UnityEngine.Object.Instantiate(chunkOriginMeshMainLoop);
                    }

                    // uv
                    {

                        if (moveUvY)
                        {

                            shiftUv++;

                            uvs = tempMesh.uv;

                            for (j = uvs.Length - 1; j >= 0; j--)
                            {
                                uvs[j].y += shiftUv;
                            }

                            tempMesh.uv = uvs;

                        }

                    }

                    // pos
                    {
                        pos = (boundsAtStartZ * 0.5f) + (spacingMeter * i) + (boundsForMainZ * (i - 1)) + (boundsForMainZ * 0.5f);
                    }

                    // set
                    {
                        combine[i].mesh = tempMesh;
                        combine[i].transform = Matrix4x4.TRS(Vector3.forward * pos, Quaternion.identity, Vector3.one);
                    }

                }

            }

            // last
            {

                Mesh tempMesh = UnityEngine.Object.Instantiate(chunkOriginMeshAtEnd);

                if (moveUvY)
                {

                    shiftUv++;

                    Vector2[] uvs = tempMesh.uv;

                    for (int j = uvs.Length - 1; j >= 0; j--)
                    {
                        uvs[j].y += shiftUv;
                    }

                    tempMesh.uv = uvs;

                }

                pos = pos + (boundsForMainZ * 0.5f) + spacingMeter + (boundsAtEndZ * 0.5f);

                combine[combineSize - 1].mesh = tempMesh;
                combine[combineSize - 1].transform = Matrix4x4.TRS(Vector3.forward * pos, Quaternion.identity, Vector3.one);

            }

            // CombineMeshes
            {
                dest.CombineMeshes(combine);
            }

            // destroy
            {

                if (Application.isPlaying)
                {
                    foreach (var val in combine)
                    {
                        UnityEngine.Object.Destroy(val.mesh);
                    }
                }

                else
                {
                    foreach (var val in combine)
                    {
                        UnityEngine.Object.DestroyImmediate(val.mesh);
                    }
                }

            }

            return true;

        }

        /// <summary>
        /// Multiply bounds by matrix
        /// </summary>
        /// <param name="bounds">bounds</param>
        /// <param name="matrix">matrix</param>
        /// <returns>bounds</returns>
        // -------------------------------------------------------------------------------------------
        public static Bounds multiplyBounds(Bounds bounds, Matrix4x4 matrix)
        {

            Vector3 min = bounds.min;
            Vector3 max = bounds.max;

            Vector3[] positions = TempBoundsPosition8;

            positions[0] = new Vector3(min.x, min.y, min.z);
            positions[1] = new Vector3(max.x, min.y, min.z);
            positions[2] = new Vector3(min.x, min.y, max.z);
            positions[3] = new Vector3(max.x, min.y, max.z);
            positions[4] = new Vector3(min.x, max.y, min.z);
            positions[5] = new Vector3(max.x, max.y, min.z);
            positions[6] = new Vector3(min.x, max.y, max.z);
            positions[7] = new Vector3(max.x, max.y, max.z);

            return GeometryUtility.CalculateBounds(TempBoundsPosition8, matrix);

        }

        /// <summary>
        /// Calculate bounds
        /// </summary>
        /// <param name="root">root GameObject</param>
        /// <returns>Bounds</returns>
        // -------------------------------------------------------------------------------------------
        public static Bounds CalcGameObjectBounds(GameObject root)
        {

            Bounds ret = new Bounds();

            // ----------------------

            if (!root)
            {
                return ret;
            }

            // ----------------------

            Vector3 min = Vector3.zero;
            Vector3 max = Vector3.zero;

            MeshFilter[] mfArray = root.GetComponentsInChildren<MeshFilter>();

            Vector3 tempMin = new Vector3(100000f, 100000f, 100000f);
            Vector3 tempMax = new Vector3(-100000f, -100000f, -100000f);

            // ----------------------

            if (mfArray.Length <= 0)
            {
                return ret;
            }

            // ----------------------

            foreach (var mf in mfArray)
            {

                tempMin = mf.sharedMesh.bounds.min;
                tempMax = mf.sharedMesh.bounds.max;

                tempMin.x *= mf.transform.lossyScale.x;
                tempMin.y *= mf.transform.lossyScale.y;
                tempMin.z *= mf.transform.lossyScale.z;

                tempMax.x *= mf.transform.lossyScale.x;
                tempMax.y *= mf.transform.lossyScale.y;
                tempMax.z *= mf.transform.lossyScale.z;

                min.x = Mathf.Min(min.x, tempMin.x);
                min.y = Mathf.Min(min.y, tempMin.y);
                min.z = Mathf.Min(min.z, tempMin.z);

                max.x = Mathf.Max(max.x, tempMax.x);
                max.y = Mathf.Max(max.y, tempMax.y);
                max.z = Mathf.Max(max.z, tempMax.z);

            }

            // SetMinMax
            {
                ret.SetMinMax(min, max);
            }

            return ret;

        }

        /// <summary>
        /// Deform mesh
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="dest">destination</param>
        /// <param name="pos0">pos0</param>
        /// <param name="pos1">pos1</param>
        /// <param name="pos2">pos2</param>
        /// <param name="pos3">pos3</param>
        /// <param name="pos4">pos4</param>
        /// <param name="pos5">pos5</param>
        /// <param name="pos6">pos6</param>
        /// <param name="pos7">pos7</param>
        /// <param name="baseTransform">base Transform</param>
        /// <param name="markNoLogerReadable">flag for UploadMeshData</param>
        // -------------------------------------------------------------------------------------------
        public static void deformMesh(
            Mesh source,
            Mesh dest,
            Vector3 pos0,
            Vector3 pos1,
            Vector3 pos2,
            Vector3 pos3,
            Vector3 pos4,
            Vector3 pos5,
            Vector3 pos6,
            Vector3 pos7,
            Transform baseTransform = null,
            bool markNoLogerReadable = false
            )
        {

            if (
                !source ||
                !dest
                )
            {
                return;
            }

            // ---------------------

            Vector3[] vertices = source.vertices;

            // ---------------------

            //
            {

                Bounds sourceBounds = source.bounds;

                Vector3 pos = Vector3.zero;

                float lerpValX = 0.0f;
                float lerpValY = 0.0f;
                float lerpValZ = 0.0f;

                Vector3 temp1 = Vector3.zero;
                Vector3 temp2 = Vector3.zero;
                Vector3 temp3 = Vector3.zero;
                Vector3 temp4 = Vector3.zero;

                for (int i = 0; i < vertices.Length; i++)
                {

                    pos = vertices[i];

                    lerpValX = Mathf.InverseLerp(sourceBounds.min.x, sourceBounds.max.x, pos.x);
                    lerpValY = Mathf.InverseLerp(sourceBounds.min.y, sourceBounds.max.y, pos.y);
                    lerpValZ = Mathf.InverseLerp(sourceBounds.min.z, sourceBounds.max.z, pos.z);

                    temp1 = Vector3.Lerp(pos0, pos1, lerpValX);
                    temp2 = Vector3.Lerp(pos2, pos3, lerpValX);
                    temp3 = Vector3.Lerp(pos4, pos5, lerpValX);
                    temp4 = Vector3.Lerp(pos6, pos7, lerpValX);

                    temp1 = Vector3.Lerp(temp1, temp2, lerpValZ);
                    temp2 = Vector3.Lerp(temp3, temp4, lerpValZ);

                    vertices[i] =
                        (baseTransform) ?
                        baseTransform.InverseTransformPoint(Vector3.Lerp(temp1, temp2, lerpValY)) :
                        Vector3.Lerp(temp1, temp2, lerpValY)
                        ;

                }

            }

            //
            {

                dest.Clear();

                dest.vertices = vertices;

                dest.uv = source.uv;
                dest.uv2 = source.uv2;
                dest.uv3 = source.uv3;
                dest.uv4 = source.uv4;
                dest.uv5 = source.uv5;
                dest.uv6 = source.uv6;
                dest.uv7 = source.uv7;
                dest.uv8 = source.uv8;
                dest.triangles = source.triangles;
                dest.normals = source.normals;
                dest.colors32 = source.colors32;

                dest.RecalculateBounds();
                dest.RecalculateNormals();

#if UNITY_2018_1_OR_NEWER
                dest.RecalculateTangents();
#endif

                dest.UploadMeshData(markNoLogerReadable);

            }

        }


        // -------------------------------------------------------------------------------------------
        // -------------------------------------------------------------------------------------------
        // -------------------------------------------------------------------------------------------


        /// <summary>
        /// Merge info
        /// </summary>
        struct MergeInfoV2
        {

            public int originalIndex;
            public int targetIndex;
            public int shift;

            public MergeInfoV2(
                int _originalIndex,
                int _targetIndex
                )
            {
                this.originalIndex = _originalIndex;
                this.targetIndex = _targetIndex;
                this.shift = 0;
            }

            public bool shouldBeReplaced()
            {
                return (this.originalIndex != this.targetIndex);
            }

            public int findTargetIndex(IList<MergeInfoV2> list)
            {

                MergeInfoV2 target = list[this.targetIndex];

                if (target.originalIndex == target.targetIndex)
                {
                    return target.originalIndex + target.shift;
                }

                else
                {
                    return target.findTargetIndex(list);
                }

            }

        }

        /// <summary>
        /// Index and position
        /// </summary>
        struct MergeIndexAndPos
        {

            public int originalIndex;
            public Vector3 position;

            public MergeIndexAndPos(int _originalIndex, Vector3 _position)
            {
                this.originalIndex = _originalIndex;
                this.position = _position;
            }

        }


        /// <summary>
        /// Merge info
        /// </summary>
        [Obsolete("Obsoleted old method", false)]
        struct MergeInfo
        {

            public int offset;
            public int target;

            public MergeInfo(int _offset, int _target)
            {
                this.offset = _offset;
                this.target = _target;
            }

        }

        /// <summary>
        /// Merge info
        /// </summary>
        class MergeVerticesInfo
        {

            public List<Vector3> tempMergeVertices = new List<Vector3>();
            public List<MergeIndexAndPos> tempMergeSortedVertices = new List<MergeIndexAndPos>();
            public List<Vector2> tempMergeUvs = new List<Vector2>();
            public List<int> tempMergeTriangles = new List<int>();
            public List<Vector3> tempMergeNormals = new List<Vector3>();
            public List<Color32> tempMergeColors = new List<Color32>();
            public List<MergeInfoV2> tempMergeInfoV2List = new List<MergeInfoV2>();

            public HashSet<int> tempAlreadyUsedIndexListForNormal = new HashSet<int>();
            public List<MergeIndexAndPos> tempMergeInfoListForNormal = new List<MergeIndexAndPos>();

            public void Clear()
            {

                this.tempMergeVertices.Clear();
                this.tempMergeSortedVertices.Clear();
                this.tempMergeUvs.Clear();
                this.tempMergeTriangles.Clear();
                this.tempMergeNormals.Clear();
                this.tempMergeColors.Clear();
                this.tempMergeInfoV2List.Clear();

                this.tempAlreadyUsedIndexListForNormal.Clear();
                this.tempMergeInfoListForNormal.Clear();

            }

        }

        static MergeVerticesInfo mergeVerticesInfo = null;

        [Obsolete]
        static List<MergeInfo> tempMergeInfoList = null;

        /// <summary>
        /// Merge vertices
        /// </summary>
        /// <param name="sourceAndDest">source and dest</param>
        /// <param name="thresholdMeter">threshold meter to merge</param>
        /// <param name="thresholdDegree">threshold degree to merge</param>
        /// <returns>success</returns>
        [Obsolete("Use mergeVerticesV2", false)]
        // -------------------------------------------------------------------------------------------
        public static bool mergeVertices(Mesh sourceAndDest, float thresholdMeter = 0.01f, float thresholdDegree = 60.0f)
        {

            if (!sourceAndDest || thresholdMeter < 0.0f)
            {
                return false;
            }

            // ------------------------------

            // clear
            {

                if (mergeVerticesInfo == null)
                {
                    mergeVerticesInfo = new MergeVerticesInfo();
                }

                else
                {
                    mergeVerticesInfo.Clear();
                }

            }

            // ------------------------------

            sourceAndDest.GetVertices(mergeVerticesInfo.tempMergeVertices);
            sourceAndDest.GetUVs(0, mergeVerticesInfo.tempMergeUvs);
            sourceAndDest.GetNormals(mergeVerticesInfo.tempMergeNormals);
            mergeVerticesInfo.tempMergeTriangles.AddRange(sourceAndDest.triangles);
            sourceAndDest.GetColors(mergeVerticesInfo.tempMergeColors);

            float thresholdMeter2 = thresholdMeter * thresholdMeter;

            // ------------------------------

            int verticeSize = sourceAndDest.vertexCount;
            int i = 0;
            int j = 0;

            // ------------------------------

            // tempMergeInfoList
            {

                int target = 0;
                int offset = 0;

                for (i = 0; i < verticeSize; i++)
                {

                    target = i;

                    // ------------

                    for (j = i - 1; j >= 0; j--)
                    {

                        if (
                            j == tempMergeInfoList[j].target &&
                            (mergeVerticesInfo.tempMergeVertices[i] - mergeVerticesInfo.tempMergeVertices[j]).sqrMagnitude <= thresholdMeter2 &&
                            Vector3.Angle(mergeVerticesInfo.tempMergeNormals[i], mergeVerticesInfo.tempMergeNormals[j]) <= thresholdDegree
                            )
                        {
                            target = j;
                            break;
                        }

                    }

                    tempMergeInfoList.Add(new MergeInfo(offset, target));

                    if (target != i)
                    {
                        offset++;
                    }

                }

            }

            // tempMergeVertices, tempMergeUvs, tempMergeNormals, tempMergeColors
            {

                if (mergeVerticesInfo.tempMergeColors.Count > 0)
                {

                    for (i = verticeSize - 1; i >= 0; i--)
                    {

                        if (i != tempMergeInfoList[i].target)
                        {
                            mergeVerticesInfo.tempMergeVertices.RemoveAt(i);
                            mergeVerticesInfo.tempMergeUvs.RemoveAt(i);
                            mergeVerticesInfo.tempMergeNormals.RemoveAt(i);
                            mergeVerticesInfo.tempMergeColors.RemoveAt(i);
                        }

                    }

                }

                else
                {

                    for (i = verticeSize - 1; i >= 0; i--)
                    {

                        if (i != tempMergeInfoList[i].target)
                        {
                            mergeVerticesInfo.tempMergeVertices.RemoveAt(i);
                            mergeVerticesInfo.tempMergeUvs.RemoveAt(i);
                            mergeVerticesInfo.tempMergeNormals.RemoveAt(i);
                            //tempMergeColors.RemoveAt(i);
                        }

                    }

                }

            }

            // tempMergeTriangles
            {

                for (i = mergeVerticesInfo.tempMergeTriangles.Count - 3; i >= 0; i -= 3)
                {

                    mergeVerticesInfo.tempMergeTriangles[i] =
                        tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i]].target -
                        tempMergeInfoList[tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i]].target].offset;

                    mergeVerticesInfo.tempMergeTriangles[i + 1] =
                        tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 1]].target -
                        tempMergeInfoList[tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 1]].target].offset;

                    mergeVerticesInfo.tempMergeTriangles[i + 2] =
                        tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 2]].target -
                        tempMergeInfoList[tempMergeInfoList[mergeVerticesInfo.tempMergeTriangles[i + 2]].target].offset;

                    if (
                        mergeVerticesInfo.tempMergeTriangles[i] == mergeVerticesInfo.tempMergeTriangles[i + 1] ||
                        mergeVerticesInfo.tempMergeTriangles[i] == mergeVerticesInfo.tempMergeTriangles[i + 2] ||
                        mergeVerticesInfo.tempMergeTriangles[i + 1] == mergeVerticesInfo.tempMergeTriangles[i + 2]
                        )
                    {
                        mergeVerticesInfo.tempMergeTriangles.RemoveRange(i, 3);
                    }

                }

            }

            // set
            {

                sourceAndDest.Clear();

                sourceAndDest.vertices = mergeVerticesInfo.tempMergeVertices.ToArray();
                sourceAndDest.uv = mergeVerticesInfo.tempMergeUvs.ToArray();
                sourceAndDest.triangles = mergeVerticesInfo.tempMergeTriangles.ToArray();
                sourceAndDest.normals = mergeVerticesInfo.tempMergeNormals.ToArray();
                sourceAndDest.colors32 = mergeVerticesInfo.tempMergeColors.ToArray();

                sourceAndDest.RecalculateBounds();
                sourceAndDest.RecalculateNormals();

#if UNITY_2018_1_OR_NEWER
                sourceAndDest.RecalculateTangents();
#endif

                sourceAndDest.UploadMeshData(false);

            }

            // clear
            {
                mergeVerticesInfo.Clear();
            }

            return true;

        }

        /// <summary>
        /// Merge vertices
        /// </summary>
        /// <param name="sourceAndDest">source and dest</param>
        /// <param name="thresholdMeter">threshold meter to merge</param>
        /// <param name="thresholdDegree">threshold degree to merge</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool mergeVerticesV2(Mesh source, Mesh dest, float thresholdMeter = 0.01f, float thresholdDegree = 60.0f)
        {

            if (
                !source ||
                !source.isReadable ||
                !dest ||
                !dest.isReadable ||
                source.vertexCount < 3
                )
            {
                return false;
            }

            // ------------------------------

            // clear
            {

                if (mergeVerticesInfo == null)
                {
                    mergeVerticesInfo = new MergeVerticesInfo();
                }

                else
                {
                    mergeVerticesInfo.Clear();
                }

            }

            // ------------------------------

            source.GetVertices(mergeVerticesInfo.tempMergeVertices);
            source.GetUVs(0, mergeVerticesInfo.tempMergeUvs);
            source.GetNormals(mergeVerticesInfo.tempMergeNormals);
            source.GetTriangles(mergeVerticesInfo.tempMergeTriangles, 0);
            source.GetColors(mergeVerticesInfo.tempMergeColors);

            for (int i = 0; i < mergeVerticesInfo.tempMergeVertices.Count; i++)
            {
                mergeVerticesInfo.tempMergeSortedVertices.Add(new MergeIndexAndPos(i, mergeVerticesInfo.tempMergeVertices[i]));
                mergeVerticesInfo.tempMergeInfoV2List.Add(new MergeInfoV2());
            }

            bool useNormal = (mergeVerticesInfo.tempMergeNormals.Count > 0);
            bool useUv = (mergeVerticesInfo.tempMergeUvs.Count > 0);
            bool useColor = (mergeVerticesInfo.tempMergeColors.Count > 0);

            // ------------------------------

            // tempSortedMergeVertices
            {
                mergeVerticesInfo.tempMergeSortedVertices.Sort((x, y) => x.position.sqrMagnitude.CompareTo(y.position.sqrMagnitude));
            }

            // find first close point
            {

                int i = 0;
                int j = 0;

                int size = mergeVerticesInfo.tempMergeVertices.Count;

                Vector3 vertexA = Vector3.zero;
                Vector3 vertexB = Vector3.zero;
                Vector3 normalA = Vector3.zero;
                Vector3 normalB = Vector3.zero;

                float thresholdMeter2 = thresholdMeter * thresholdMeter;

                int targetIndex = 0;

                float sqrMagnitude = 0.0f;

                MergeIndexAndPos indexAndPos;

                for (i = 0; i < size; i++)
                {

                    indexAndPos = mergeVerticesInfo.tempMergeSortedVertices[i];

                    vertexA = indexAndPos.position;

                    if (useNormal)
                    {
                        normalA = mergeVerticesInfo.tempMergeNormals[indexAndPos.originalIndex];
                    }

                    targetIndex = indexAndPos.originalIndex;

                    for (j = i - 1; j >= 0; j--)
                    {

                        vertexB = mergeVerticesInfo.tempMergeSortedVertices[j].position;

                        if (useNormal)
                        {
                            normalB = mergeVerticesInfo.tempMergeNormals[mergeVerticesInfo.tempMergeSortedVertices[j].originalIndex];
                        }

                        sqrMagnitude = (vertexA - vertexB).sqrMagnitude;

                        if (sqrMagnitude <= thresholdMeter2)
                        {

                            if (!useNormal || Vector3.Angle(normalA, normalB) <= thresholdDegree)
                            {
                                targetIndex = mergeVerticesInfo.tempMergeSortedVertices[j].originalIndex;
                                break;
                            }

                        }

                        // NOTE
                        // to be exact (vertexB.magnitude + thresholdMeter < vertexA.magnitude)
                        else if (vertexB.sqrMagnitude + thresholdMeter2 < vertexA.sqrMagnitude)
                        {
                            break;
                        }

                    }

                    mergeVerticesInfo.tempMergeInfoV2List[indexAndPos.originalIndex] = new MergeInfoV2(indexAndPos.originalIndex, targetIndex);

                }

            }

            // shift
            {

                MergeInfoV2 temp;

                int shift = 0;

                for (int i = 0; i < mergeVerticesInfo.tempMergeInfoV2List.Count; i++)
                {

                    temp = mergeVerticesInfo.tempMergeInfoV2List[i];

                    if (temp.originalIndex != temp.targetIndex)
                    {
                        shift--;
                    }

                    temp.shift = shift;

                    mergeVerticesInfo.tempMergeInfoV2List[i] = temp;

                }

            }

            // vertex, uv, color
            {

                for (int i = mergeVerticesInfo.tempMergeVertices.Count - 1; i >= 0; i--)
                {

                    if (mergeVerticesInfo.tempMergeInfoV2List[i].shouldBeReplaced())
                    {

                        mergeVerticesInfo.tempMergeVertices.RemoveAt(i);

                        if (useUv)
                        {
                            mergeVerticesInfo.tempMergeUvs.RemoveAt(i);
                        }

                        if (useNormal)
                        {
                            mergeVerticesInfo.tempMergeNormals.RemoveAt(i);
                        }

                        if (useColor)
                        {
                            mergeVerticesInfo.tempMergeColors.RemoveAt(i);
                        }

                    }

                }

            }

            // triangle
            {

                int val0 = 0;
                int val1 = 0;
                int val2 = 0;

                for (int i = mergeVerticesInfo.tempMergeTriangles.Count - 3; i >= 0; i -= 3)
                {

                    val0 = mergeVerticesInfo.tempMergeInfoV2List[mergeVerticesInfo.tempMergeTriangles[i + 0]].findTargetIndex(mergeVerticesInfo.tempMergeInfoV2List);
                    val1 = mergeVerticesInfo.tempMergeInfoV2List[mergeVerticesInfo.tempMergeTriangles[i + 1]].findTargetIndex(mergeVerticesInfo.tempMergeInfoV2List);
                    val2 = mergeVerticesInfo.tempMergeInfoV2List[mergeVerticesInfo.tempMergeTriangles[i + 2]].findTargetIndex(mergeVerticesInfo.tempMergeInfoV2List);

                    // 

                    if (
                        val0 == val1 ||
                        val0 == val2 ||
                        val1 == val2
                        )
                    {
                        mergeVerticesInfo.tempMergeTriangles.RemoveRange(i, 3);
                    }

                    else
                    {
                        mergeVerticesInfo.tempMergeTriangles[i + 0] = val0;
                        mergeVerticesInfo.tempMergeTriangles[i + 1] = val1;
                        mergeVerticesInfo.tempMergeTriangles[i + 2] = val2;
                    }

                }

            }

            // set
            {

                dest.Clear();

                dest.SetVertices(mergeVerticesInfo.tempMergeVertices);
                dest.SetUVs(0, mergeVerticesInfo.tempMergeUvs);
                dest.SetNormals(mergeVerticesInfo.tempMergeNormals);
                dest.SetTriangles(mergeVerticesInfo.tempMergeTriangles, 0);
                dest.SetColors(mergeVerticesInfo.tempMergeColors);

                dest.RecalculateBounds();
                dest.RecalculateNormals();

#if UNITY_2018_1_OR_NEWER
                dest.RecalculateTangents();
#endif

                dest.UploadMeshData(false);

            }

            // clear
            {
                mergeVerticesInfo.Clear();
            }

            return true;

        }

        /// <summary>
        /// Smooth normals
        /// </summary>
        /// <param name="sourceAndDest">source and dest</param>
        /// <param name="thresholdMeter">threshold meter to merge</param>
        /// <param name="thresholdDegree">threshold degree to merge</param>
        /// <returns>success</returns>
        // -------------------------------------------------------------------------------------------
        public static bool smoothNormals(Mesh sourceAndDest, float thresholdMeter = 0.01f, float thresholdDegree = 60.0f)
        {

            if (
                !sourceAndDest ||
                !sourceAndDest.isReadable ||
                sourceAndDest.vertexCount < 3
                )
            {
                return false;
            }

            // ------------------------------

            // clear
            {

                if (mergeVerticesInfo == null)
                {
                    mergeVerticesInfo = new MergeVerticesInfo();
                }

                else
                {
                    mergeVerticesInfo.Clear();
                }

            }

            // ------------------------------

            sourceAndDest.GetVertices(mergeVerticesInfo.tempMergeVertices);
            sourceAndDest.GetNormals(mergeVerticesInfo.tempMergeNormals);

            for (int i = 0; i < mergeVerticesInfo.tempMergeVertices.Count; i++)
            {
                mergeVerticesInfo.tempMergeSortedVertices.Add(new MergeIndexAndPos(i, mergeVerticesInfo.tempMergeVertices[i]));
            }

            // ------------------------------

            //
            {
                if (mergeVerticesInfo.tempMergeNormals.Count <= 0)
                {
                    return false;
                }
            }

            // tempSortedMergeVertices
            {
                mergeVerticesInfo.tempMergeSortedVertices.Sort((x, y) => x.position.sqrMagnitude.CompareTo(y.position.sqrMagnitude));
            }

            //
            {

                int i = 0;
                int j = 0;

                int size = mergeVerticesInfo.tempMergeVertices.Count;

                Vector3 normalA = Vector3.zero;
                Vector3 normalB = Vector3.zero;

                Vector3 averageNormal = Vector3.zero;

                MergeIndexAndPos valA;
                MergeIndexAndPos valB;

                float thresholdMeter2 = thresholdMeter * thresholdMeter;
                float sqrMagnitude = 0.0f;

                for (i = 0; i < size; i++)
                {

                    if (!mergeVerticesInfo.tempAlreadyUsedIndexListForNormal.Add(i))
                    {
                        continue;
                    }

                    // ---------------

                    valA = mergeVerticesInfo.tempMergeSortedVertices[i];

                    normalA = mergeVerticesInfo.tempMergeNormals[valA.originalIndex];

                    mergeVerticesInfo.tempMergeInfoListForNormal.Add(valA);

                    // ---------------

                    for (j = i + 1; j < size; j++)
                    {

                        if (mergeVerticesInfo.tempAlreadyUsedIndexListForNormal.Contains(j))
                        {
                            continue;
                        }

                        // ---------------

                        valB = mergeVerticesInfo.tempMergeSortedVertices[j];

                        normalB = mergeVerticesInfo.tempMergeNormals[valB.originalIndex];

                        sqrMagnitude = (valA.position - valB.position).sqrMagnitude;

                        // ---------------

                        if (
                            sqrMagnitude <= thresholdMeter2 &&
                            Vector3.Angle(normalA, normalB) <= thresholdDegree
                            )
                        {
                            mergeVerticesInfo.tempMergeInfoListForNormal.Add(valB);
                            mergeVerticesInfo.tempAlreadyUsedIndexListForNormal.Add(j);
                        }

                        // NOTE
                        // to be exact (valB.position.magnitude - valA.position.magnitude > thresholdMeter)
                        else if (valB.position.sqrMagnitude - valA.position.sqrMagnitude > thresholdMeter2)
                        {
                            break;
                        }

                    }

                    if (mergeVerticesInfo.tempMergeInfoListForNormal.Count > 1)
                    {

                        averageNormal = Vector3.zero;

                        for (j = 0; j < mergeVerticesInfo.tempMergeInfoListForNormal.Count; j++)
                        {
                            averageNormal += mergeVerticesInfo.tempMergeNormals[mergeVerticesInfo.tempMergeInfoListForNormal[j].originalIndex];
                        }

                        averageNormal = (averageNormal / mergeVerticesInfo.tempMergeInfoListForNormal.Count).normalized;

                        for (j = 0; j < mergeVerticesInfo.tempMergeInfoListForNormal.Count; j++)
                        {
                            mergeVerticesInfo.tempMergeNormals[mergeVerticesInfo.tempMergeInfoListForNormal[j].originalIndex] = averageNormal;
                        }

                    }

                    mergeVerticesInfo.tempMergeInfoListForNormal.Clear();

                }

            }

            // set
            {
                sourceAndDest.SetNormals(mergeVerticesInfo.tempMergeNormals);
                sourceAndDest.UploadMeshData(false);
            }

            // clear
            {
                mergeVerticesInfo.Clear();
            }

            return true;

        }

    }

}
