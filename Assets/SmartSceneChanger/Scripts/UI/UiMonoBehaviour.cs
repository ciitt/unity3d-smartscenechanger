﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// UI MonoBehaviour
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class UiMonoBehaviour : MonoBehaviour
    {

        /// <summary>
        /// Reference to RectTransform
        /// </summary>
        RectTransform m_refRectTransform = null;

        // -----------------------------------------------------------------------------------------------

        /// <summary>
        /// Reference to RectTransform
        /// </summary>
        public RectTransform refRectTransform
        {

            get
            {

                if (!this.m_refRectTransform)
                {
                    this.m_refRectTransform = this.transform as RectTransform;
                }

                return this.m_refRectTransform;

            }

        }

    }

}
