﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Recycle scroll view UI element
    /// </summary>
    public abstract class RecycleScrollViewUiElement<T> : UiMonoBehaviour where T : RecycleScrollViewUiContent
    {

        /// <summary>
        /// Reference to resize target
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to resize target")]
        protected RectTransform m_refResizeTarget = null;

        /// <summary>
        /// Parent RecycleScrollUiGroup
        /// </summary>
        protected RecycleScrollViewUiGroup<T> m_refParentGroup = null;

        /// <summary>
        /// Reference to currrent content
        /// </summary>
        protected T m_refCurrentContent = null;

        /// <summary>
        /// Set content
        /// </summary>
        /// <typeparam name="T">RecycleScrollViewUiContent</typeparam>
        /// <param name="contentOrNull">content or null</param>
        public abstract void setContent(T contentOrNull);

        // -----------------------------------------------------------------------------------------------

        /// <summary>
        /// Parent RecycleScrollUiGroup
        /// </summary>
        public RecycleScrollViewUiGroup<T> refParentGroup
        {
            get { return this.m_refParentGroup; }
        }

        /// <summary>
        /// Reference to currrent content
        /// </summary>
        public T refCurrentContent
        {
            get { return this.m_refCurrentContent; }
        }

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            // m_refParentGroup
            {
                this.m_refParentGroup = this.GetComponentInParent<RecycleScrollViewUiGroup<T>>();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refResizeTarget)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refResizeTarget is null : " + Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refParentGroup)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refParentGroup is null : " + Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// Update element
        /// </summary>
        /// <param name="content">content</param>
        /// <param name="position">position</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void updateElement(T content, Vector2 position)
        {

            // m_refCurrentContent
            {
                this.m_refCurrentContent = content;
            }

            // SetActive
            {

                if (content == null && this.gameObject.activeSelf)
                {
                    this.gameObject.SetActive(false);
                    return;
                }

                else if (content != null && !this.gameObject.activeSelf)
                {
                    this.gameObject.SetActive(true);
                }

            }

            // ---------------------------

            // anchoredPosition
            {

                if (content != null)
                {
                    (this.transform as RectTransform).anchoredPosition = position + content.currentLayoutOffset;
                }

                else
                {
                    (this.transform as RectTransform).anchoredPosition = position;
                }
                
            }

            // setContent
            {
                this.setContent(content as T);
            }

        }

        /// <summary>
        /// Delete content
        /// </summary>
        /// <param name="transitionSeconds">transition seconds</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void deleteContent(float transitionSeconds)
        {

            if (this.m_refParentGroup)
            {
                this.m_refParentGroup.deleteContent(this.m_refCurrentContent, transitionSeconds);
            }

        }

        /// <summary>
        /// Resize content
        /// </summary>
        /// <param name="sizeDelta">target size</param>
        /// <param name="transitionSeconds">transition seconds</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void resizeContent(Vector2 sizeDelta, float transitionSeconds)
        {

            if (this.m_refParentGroup)
            {
                this.m_refParentGroup.resizeContent(this.m_refCurrentContent, sizeDelta, transitionSeconds);
            }

        }

        /// <summary>
        /// Resize this if needed
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual void resizeThisIfNeeded()
        {

            if (
                this.m_refResizeTarget &&
                this.m_refCurrentContent != null
                )
            {

                Vector2 sizeDelta = this.m_refResizeTarget.sizeDelta;

                if ((sizeDelta - this.m_refCurrentContent.uiSizeDelta).sqrMagnitude > 0.0f)
                {
                    this.m_refResizeTarget.sizeDelta = this.m_refCurrentContent.uiSizeDelta;
                }

            }

        }

    }

}
