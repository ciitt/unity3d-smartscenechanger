﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SSC
{

    /// <summary>
    /// Recycle scroll view UI group
    /// </summary>
    [RequireComponent(typeof(ScrollRect))]
    public class RecycleScrollViewUiGroup<T> : UiMonoBehaviour where T : RecycleScrollViewUiContent
    {

        /// <summary>
        /// Scroll direction
        /// </summary>
        public enum ScrollDirection
        {
            BottomToTop,
            LeftToRight
        }

        /// <summary>
        /// Scroll direction
        /// </summary>
        [SerializeField]
        [Tooltip("Scroll direction")]
        protected ScrollDirection m_scrollDirection = ScrollDirection.BottomToTop;

        /// <summary>
        /// Reference to ScrollRect
        /// </summary>
        protected ScrollRect m_refScrollRect = null;

        /// <summary>
        /// Element UI list
        /// </summary>
        protected RecycleScrollViewUiElement<T>[] m_refElementUis = new RecycleScrollViewUiElement<T>[0];

        /// <summary>
        /// Current content list
        /// </summary>
        protected List<T> m_currentContentList = new List<T>();

        /// <summary>
        /// Previous content position
        /// </summary>
        protected Vector2 m_previousContentPosition = Vector2.zero;

        /// <summary>
        /// IEnumerator for any
        /// </summary>
        protected IEnumerator m_resizeOrDeleteContentIE = null;

        /// <summary>
        /// Now resizing content counter
        /// </summary>
        protected int m_nowResizingContentCounter = 0;

        /// <summary>
        /// Use vertical scroll
        /// </summary>
        protected bool m_useVerticalScroll = false;

        /// <summary>
        /// Use horizontal scroll
        /// </summary>
        protected bool m_useHorizontalScroll = false;

        /// <summary>
        /// Force update elements flag
        /// </summary>
        protected bool m_forceUpdateElementsFlag = true;

        // -----------------------------------------------------------------------------------------------

        /// <summary>
        /// Current content list
        /// </summary>
        public IList<T> currentContentList
        {
            get { return this.m_currentContentList; }
        }

        /// <summary>
        /// Awake
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            // m_refScrollRect
            {
                this.m_refScrollRect = this.GetComponent<ScrollRect>();
            }

            // m_refElementUis
            {

                if (this.m_refScrollRect.content)
                {
                    this.m_refElementUis = this.m_refScrollRect.content.GetComponentsInChildren<RecycleScrollViewUiElement<T>>(true);
                }

            }

            // m_useVerticalScroll, m_useHorizontalScroll
            {
                this.m_useVerticalScroll = this.m_refScrollRect.vertical;
                this.m_useHorizontalScroll = this.m_refScrollRect.horizontal;
            }

#if UNITY_EDITOR

            //
            {

                if (this.m_refScrollRect.movementType != ScrollRect.MovementType.Clamped)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refScrollRect.movementType != ScrollRect.MovementType.Clamped : " + Funcs.CreateHierarchyPath(this.transform));
                }

                if (!this.m_refScrollRect.content)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refScrollRect.content is null : " + Funcs.CreateHierarchyPath(this.transform));
                }

                if (
                    this.m_scrollDirection == ScrollDirection.BottomToTop &&
                    this.m_refScrollRect.verticalScrollbar &&
                    this.m_refScrollRect.verticalScrollbar.direction != Scrollbar.Direction.BottomToTop
                    )
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refScrollRect.verticalScrollbar.direction != Scrollbar.Direction.BottomToTop : " + Funcs.CreateHierarchyPath(this.transform));
                }

                if (
                    this.m_scrollDirection == ScrollDirection.LeftToRight &&
                    this.m_refScrollRect.horizontalScrollbar &&
                    this.m_refScrollRect.horizontalScrollbar.direction != Scrollbar.Direction.LeftToRight
                    )
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refScrollRect.horizontalScrollbar.direction != Scrollbar.Direction.LeftToRight : " + Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // UpdateUiElements
            {
                this.updateUiElements(this.m_forceUpdateElementsFlag);
            }

            // resizeAllElementsIfNeeded
            {
                this.resizeAllElementsIfNeeded();
            }

            // m_previousContentPosition
            {

                if (this.m_refScrollRect.content)
                {
                    this.m_previousContentPosition = this.m_refScrollRect.content.anchoredPosition;
                }

            }

            // m_forceUpdateElementsFlag
            {
                this.m_forceUpdateElementsFlag = false;
            }

        }

        /// <summary>
        /// Find element
        /// </summary>
        /// <param name="content">content</param>
        /// <returns>element</returns>
        // -----------------------------------------------------------------------------------------------
        public virtual RecycleScrollViewUiElement<T> findElement(RecycleScrollViewUiContent content)
        {

            foreach (var val in this.m_refElementUis)
            {

                if (val && val.refCurrentContent == content)
                {
                    return val;
                }

            }

            return null;

        }

        /// <summary>
        /// Resize all elements if needed
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        protected virtual void resizeAllElementsIfNeeded()
        {

            foreach (var val in this.m_refElementUis)
            {
                if (val)
                {
                    val.resizeThisIfNeeded();
                }
            }

        }

        /// <summary>
        /// Update ui elements
        /// </summary>
        /// <param name="forcibly">forcibly</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void updateUiElements(bool forcibly)
        {

            if (
                !forcibly &&
                !this.shouldUpdateLayout()
                )
            {
                return;
            }

            // -------------------------------
            
            // update
            {

                if (this.m_scrollDirection == ScrollDirection.BottomToTop)
                {
                    this.updateUiElementsBottomToTop();
                }

                else
                {
                    this.updateUiElementsLeftToRight();
                }

            }

            // clamp scrollbar
            {

                float value = 0.0f;

                if (this.m_refScrollRect.verticalScrollbar)
                {

                    value = this.m_refScrollRect.verticalScrollbar.value;

                    if (value < -0.00001f || 1.00001f < value)
                    {
                        this.m_refScrollRect.verticalScrollbar.value = Mathf.Lerp(value, Mathf.Clamp01(this.m_refScrollRect.verticalScrollbar.value), 0.1f);
                    }
                }

                if (this.m_refScrollRect.horizontalScrollbar)
                {

                    value = this.m_refScrollRect.horizontalScrollbar.value;

                    if (value < -0.00001f || 1.00001f < value)
                    {
                        this.m_refScrollRect.horizontalScrollbar.value = Mathf.Lerp(value, Mathf.Clamp01(this.m_refScrollRect.horizontalScrollbar.value), 0.1f);
                    }

                }

            }

        }

        /// <summary>
        /// Update ui elements bottom to top
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual void updateUiElementsBottomToTop()
        {

            if (!this.m_refScrollRect.content)
            {
                return;
            }

            // -------------------------------

            int startIndex = -1000;
            float startPos = 0.0f;
            float currentTotalSize = 0.0f;

            // -------------------------------

            // startIndex
            {

                RecycleScrollViewUiContent temp = null;

                Vector2 allContentSizeDelta = Vector2.zero;

                float posY = Mathf.Abs(this.m_refScrollRect.content.anchoredPosition.y);

                for (int i = 0; i < this.m_currentContentList.Count; i++)
                {

                    temp = this.m_currentContentList[i];

                    if (temp != null)
                    {

                        if (startIndex <= -1000 && currentTotalSize + temp.uiSizeDelta.y + 1 >= posY)
                        {
                            startIndex = i;
                            startPos = currentTotalSize;
                        }

                        currentTotalSize += temp.uiSizeDelta.y;

                    }

                }

                // currentTotalSize
                {
                    Vector2 sizeDelta = this.m_refScrollRect.content.sizeDelta;
                    sizeDelta.y = currentTotalSize;
                    this.m_refScrollRect.content.sizeDelta = sizeDelta;
                }

            }

            // UpdateElement
            {

                int currentContentIndex = startIndex;

                RecycleScrollViewUiElement<T> element = null;
                T content = null;

                Vector2 position = new Vector2(0, -startPos);

                for (int i = 0; i < this.m_refElementUis.Length; i++)
                {

                    element = this.m_refElementUis[i];

                    content =
                        (currentContentIndex < 0 || this.m_currentContentList.Count <= currentContentIndex) ?
                        null :
                        this.m_currentContentList[currentContentIndex]
                        ;

                    // --------------------

                    if (!element)
                    {
                        continue;
                    }

                    // --------------------

                    // UpdateElement
                    {

                        element.updateElement(content, position);

                        if (content != null)
                        {
                            position.y -= content.uiSizeDelta.y;
                        }

                    }

                    //
                    {
                        currentContentIndex++;
                    }

                }

            }

        }

        /// <summary>
        /// Update ui elements left to right
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual void updateUiElementsLeftToRight()
        {

            if (!this.m_refScrollRect.content)
            {
                return;
            }

            // -------------------------------

            int startIndex = -1000;
            float startPos = 0.0f;
            float currentTotalSize = 0.0f;

            // -------------------------------

            // startIndex
            {

                RecycleScrollViewUiContent temp = null;

                Vector2 allContentSizeDelta = Vector2.zero;

                float posX = Mathf.Abs(this.m_refScrollRect.content.anchoredPosition.x);

                for (int i = 0; i < this.m_currentContentList.Count; i++)
                {

                    temp = this.m_currentContentList[i];

                    if (temp != null)
                    {

                        if (startIndex <= -1000 && currentTotalSize + temp.uiSizeDelta.x + 1 >= posX)
                        {
                            startIndex = i;
                            startPos = currentTotalSize;
                        }

                        currentTotalSize += temp.uiSizeDelta.x;

                    }

                }

                // currentTotalSize
                {
                    Vector2 sizeDelta = this.m_refScrollRect.content.sizeDelta;
                    sizeDelta.x = currentTotalSize;
                    this.m_refScrollRect.content.sizeDelta = sizeDelta;
                }

            }

            // UpdateElement
            {

                int currentContentIndex = startIndex;

                RecycleScrollViewUiElement<T> element = null;
                T content = null;

                Vector2 position = new Vector2(startPos, 0);

                for (int i = 0; i < this.m_refElementUis.Length; i++)
                {

                    element = this.m_refElementUis[i];

                    content =
                        (currentContentIndex < 0 || this.m_currentContentList.Count <= currentContentIndex) ?
                        null :
                        this.m_currentContentList[currentContentIndex]
                        ;

                    // -------------

                    if (!element)
                    {
                        continue;
                    }

                    // --------------------

                    // UpdateElement
                    {

                        element.updateElement(content, position);

                        if (content != null)
                        {
                            position.x += content.uiSizeDelta.x;
                        }

                    }

                    //
                    {
                        currentContentIndex++;
                    }

                }

            }

        }

        /// <summary>
        /// Should update layout
        /// </summary>
        /// <returns>yes</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual bool shouldUpdateLayout()
        {

            if (this.m_refScrollRect.content)
            {
                return
                    (this.m_refScrollRect.content.anchoredPosition - this.m_previousContentPosition).sqrMagnitude > 0.0f ||
                    this.m_resizeOrDeleteContentIE != null
                    ;
            }

            else
            {
                return this.m_resizeOrDeleteContentIE != null;
            }

        }

        /// <summary>
        /// Set contents
        /// </summary>
        /// <param name="content">content</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void setContents(IList<T> contentList)
        {

            this.stopScrollMovement();

            this.m_currentContentList.Clear();

            this.m_currentContentList.AddRange(contentList);

            this.m_forceUpdateElementsFlag = true;

        }

        /// <summary>
        /// Add content
        /// </summary>
        /// <param name="content">content</param>
        /// <param name="updateUiLayout">update ui layout</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void addContent(T content, bool updateUiLayout)
        {

            this.stopScrollMovement();

            this.m_currentContentList.Add(content);

            this.m_forceUpdateElementsFlag = true;

        }

        /// <summary>
        /// Delete content
        /// </summary>
        /// <param name="content">content</param>
        /// <param name="transitionSeconds">transition seconds</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void deleteContent(T content, float transitionSeconds)
        {

            if (this.m_resizeOrDeleteContentIE == null)
            {
                StartCoroutine(this.m_resizeOrDeleteContentIE = this.deleteContentIE(content, transitionSeconds));
            }

        }

        /// <summary>
        /// Resize content
        /// </summary>
        /// <param name="content">content</param>
        /// <param name="sizeDelta">target size</param>
        /// <param name="transitionSeconds">transition seconds</param>
        // -----------------------------------------------------------------------------------------------
        public virtual void resizeContent(RecycleScrollViewUiContent content, Vector2 sizeDelta, float transitionSeconds)
        {

            if (this.m_resizeOrDeleteContentIE == null)
            {
                StartCoroutine(this.m_resizeOrDeleteContentIE = this.resizeContentIE(content, sizeDelta, transitionSeconds));
            }

        }

        /// <summary>
        /// Delete content internal  IE
        /// </summary>
        /// <param name="content">target content</param>
        /// <param name="transitionSeconds">transition seconds</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator deleteContentInternalIE(RecycleScrollViewUiContent content, float transitionSeconds)
        {

            // resize
            {

                float val = 0.0f;
                float timer = 0.0f;

                Vector2 from = content.uiSizeDelta;
                Vector2 to =
                    (this.m_scrollDirection == ScrollDirection.BottomToTop) ?
                    new Vector2(from.x, 0) :
                    new Vector2(0, from.y)
                    ;

                while (timer < transitionSeconds)
                {

                    val = timer / transitionSeconds;

                    content.uiSizeDelta = Vector2.Lerp(from, to, val * val);

                    timer += Time.deltaTime;

                    yield return null;

                }

                content.uiSizeDelta = to;

            }

        }

        /// <summary>
        /// Delete content IE
        /// </summary>
        /// <param name="content">target content</param>
        /// <param name="transitionSeconds">transition seconds</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator deleteContentIE(T content, float transitionSeconds)
        {

            if (content == null)
            {
                this.m_resizeOrDeleteContentIE = null;
                yield break;
            }

            // --------------------------

            // init
            {
                this.stopScrollMovement();
                this.setScrollbarActive(false);
            }

            // ---------------------------

            // deleteContentInternalIE
            {
                yield return this.deleteContentInternalIE(content, transitionSeconds);
            }

            // finish
            {

                this.m_currentContentList.Remove(content);
                
                yield return null;

                this.m_resizeOrDeleteContentIE = null;

                this.setScrollbarActive(true);

            }

        }

        /// <summary>
        /// Resize IE
        /// </summary>
        /// <param name="content">target content</param>
        /// <param name="transitionSeconds">transition seconds</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator resizeContentInternalIE(RecycleScrollViewUiContent content, Vector2 sizeDelta, float transitionSeconds)
        {

            // resize
            {

                float val = 0.0f;
                float timer = 0.0f;

                Vector2 firstSize = content.uiSizeDelta;

                while (timer < transitionSeconds)
                {

                    val = timer / transitionSeconds;

                    content.uiSizeDelta = Vector2.Lerp(firstSize, sizeDelta, val * val);

                    timer += Time.deltaTime;

                    yield return null;

                }

            }

        }

        /// <summary>
        /// Resize IE
        /// </summary>
        /// <param name="content">target content</param>
        /// <param name="transitionSeconds">transition seconds</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected virtual IEnumerator resizeContentIE(RecycleScrollViewUiContent content, Vector2 sizeDelta, float transitionSeconds)
        {

            if (content == null)
            {
                this.m_resizeOrDeleteContentIE = null;
                yield break;
            }

            // --------------------------

            // init
            {
                this.stopScrollMovement();
                this.setScrollbarActive(false);
            }

            // resizeContentInternalIE
            {
                yield return this.resizeContentInternalIE(content, sizeDelta, transitionSeconds);
            }

            // finish
            {

                content.uiSizeDelta = sizeDelta;
                
                yield return null;

                this.m_resizeOrDeleteContentIE = null;

                this.setScrollbarActive(true);

            }

        }

        /// <summary>
        /// Stop scroll movement
        /// </summary>
        // -----------------------------------------------------------------------------------------------
        public virtual void stopScrollMovement()
        {

            if (this.m_refScrollRect)
            {
                this.m_refScrollRect.StopMovement();
            }

        }

        /// <summary>
        /// Set scroll bar active
        /// </summary>
        /// <param name="active">active</param>
        // -----------------------------------------------------------------------------------------------
        protected virtual void setScrollbarActive(bool active)
        {

            if (this.m_refScrollRect)
            {

                if (
                    this.m_useVerticalScroll &&
                    this.m_refScrollRect.vertical != active
                    )
                {
                    this.m_refScrollRect.vertical = active;
                }

                if (
                    this.m_useHorizontalScroll &&
                    this.m_refScrollRect.horizontal != active
                    )
                {
                    this.m_refScrollRect.horizontal = active;
                }

            }

        }

    }

}
