﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Recycle scroll view UI group
    /// </summary>
    public class CustomDeleteRecycleScrollViewUiGroup<T> : RecycleScrollViewUiGroup<T> where T : RecycleScrollViewUiContent
    {

        /// <summary>
        /// Offset when delete a content
        /// </summary>
        [SerializeField]
        [Tooltip("Offset when delete a content")]
        protected Vector2 m_deleteContentOffset = new Vector2(100, 0);

        /// <summary>
        /// Delete content internal  IE
        /// </summary>
        /// <param name="content">target content</param>
        /// <param name="transitionSeconds">transition seconds</param>
        /// <returns>IEnumerator</returns>
        // -----------------------------------------------------------------------------------------------
        protected override IEnumerator deleteContentInternalIE(RecycleScrollViewUiContent content, float transitionSeconds)
        {

            // offset
            {

                float val = 0.0f;
                float timer = 0.0f;

                Vector2 from = content.currentLayoutOffset;
                Vector2 to = this.m_deleteContentOffset;

                while (timer < transitionSeconds)
                {

                    val = timer / transitionSeconds;

                    content.currentLayoutOffset = Vector2.Lerp(from, to, val * val);

                    timer += Time.deltaTime;

                    yield return null;

                }

                content.currentLayoutOffset = to;

            }


            // base
            {
                yield return base.deleteContentInternalIE(content, transitionSeconds);
            }

        }

    }

}
