﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// Recycle scroll view UI content
    /// </summary>
    public abstract class RecycleScrollViewUiContent
    {

        /// <summary>
        /// UI sizeDelta
        /// </summary>
        public Vector2 uiSizeDelta = new Vector2(100, 100);

        /// <summary>
        /// Current layout offset
        /// </summary>
        [NonSerialized]
        public Vector2 currentLayoutOffset = Vector2.zero;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_uiSizeDelta">uiSizeDelta</param>
        public RecycleScrollViewUiContent(Vector2 _uiSizeDelta)
        {
            this.uiSizeDelta = _uiSizeDelta;
        }

    }

}
