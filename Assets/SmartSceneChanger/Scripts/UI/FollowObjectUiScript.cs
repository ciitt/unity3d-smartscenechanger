﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// UI to follow an object
    /// </summary>
    public abstract class FollowObjectUiScript : UiMonoBehaviour
    {

        /// <summary>
        /// Reference to Camera
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Camera")]
        protected Camera m_refCamera = null;

        /// <summary>
        /// Reference to Transform to follow
        /// </summary>
        [SerializeField]
        [Tooltip("Reference to Transform to follow")]
        protected Transform m_refFollowTarget = null;

        /// <summary>
        /// Stay on screen border
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border")]
        protected bool m_stayOnScreenBorder = false;

        /// <summary>
        /// Stay on screen border offset X
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border offset X")]
        [Range(0.0f, 0.5f)]
        protected float m_stayOnScreenBorderOffsetX = 0.045f;

        /// <summary>
        /// Stay on screen border offset Y
        /// </summary>
        [SerializeField]
        [Tooltip("Stay on screen border offset Y")]
        [Range(0.0f, 0.5f)]
        protected float m_stayOnScreenBorderOffsetY = 0.08f;

        /// <summary>
        /// Reference to Canvas RectTransform
        /// </summary>
        protected RectTransform m_refCanvasRectTransform = null;

        /// <summary>
        /// IEnumerator for show or hide 
        /// </summary>
        protected IEnumerator m_showOrHideIE = null;

        /// <summary>
        /// Now showing
        /// </summary>
        protected bool m_nowShowing = true;

        // ------------------------------------------------------------------------------------------

        /// <summary>
        /// Show IEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        protected abstract IEnumerator show();

        /// <summary>
        /// Hide IEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        protected abstract IEnumerator hide();

        /// <summary>
        /// Start
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Start()
        {

            // m_refCanvasRectTransform
            {
                this.m_refCanvasRectTransform = this.GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            }

#if UNITY_EDITOR

            //
            {

                if (!this.m_refCamera)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : m_refCamera == null : " + Funcs.CreateHierarchyPath(this.transform));
                }

                if (this.refRectTransform.anchorMin != Vector2.zero || this.refRectTransform.anchorMax != Vector2.zero)
                {
                    Debug.LogWarning("(#if UNITY_EDITOR) : refRectTransform.anchorMin, anchorMax != Vector2.zero : (Bottom Left) : " + Funcs.CreateHierarchyPath(this.transform));
                }

            }

#endif

        }

        /// <summary>
        /// LateUpdate
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void LateUpdate()
        {

            // UpdatePosition
            {
                this.UpdatePosition();
            }

        }

        /// <summary>
        /// Set target to follow
        /// </summary>
        /// <param name="target">target</param>
        // ------------------------------------------------------------------------------------------
        public void SetFollowTarget(Transform target)
        {
            this.m_refFollowTarget = target;
        }

        /// <summary>
        /// Start showing if needed
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void startShowingIfNeeded()
        {

            if (!this.m_nowShowing)
            {

                this.m_nowShowing = true;

                if (this.m_showOrHideIE != null)
                {
                    StopCoroutine(this.m_showOrHideIE);
                    this.m_showOrHideIE = null;
                }

                StartCoroutine(this.m_showOrHideIE = this.showBase());

            }

        }

        /// <summary>
        /// Start showing if needed
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void startHidingIfNeeded()
        {

            if (this.m_nowShowing)
            {

                this.m_nowShowing = false;

                if (this.m_showOrHideIE != null)
                {
                    StopCoroutine(this.m_showOrHideIE);
                    this.m_showOrHideIE = null;
                }

                StartCoroutine(this.m_showOrHideIE = this.hideBase());

            }

        }

        /// <summary>
        /// Show IEnumerator base
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator showBase()
        {

            yield return this.show();

            this.m_showOrHideIE = null;

        }

        /// <summary>
        /// Hide IEnumerator base
        /// </summary>
        /// <returns>IEnumerator</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual IEnumerator hideBase()
        {

            yield return this.hide();

            this.m_showOrHideIE = null;

        }

        /// <summary>
        /// Should show UI
        /// </summary>
        /// <param name="normalizedX">normalized X</param>
        /// <param name="normalizedY">normalized Y</param>
        /// <param name="targetObjectIsBehindCamera">target object is behind camera</param>
        /// <returns>yes</returns>
        // ------------------------------------------------------------------------------------------
        protected virtual bool ShouldShowUi(
            float normalizedX,
            float normalizedY,
            bool targetObjectIsBehindCamera
            )
        {
            return !targetObjectIsBehindCamera;
        }

        /// <summary>
        /// Update position
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void UpdatePosition()
        {

            if (!this.m_refCamera)
            {
                return;
            }

            if (!this.m_refFollowTarget)
            {
                this.startHidingIfNeeded();
                return;
            }

            // -----------------------

            // pos
            {

                Vector3 viewportPoint = this.m_refCamera.WorldToViewportPoint(this.m_refFollowTarget.position);
                Vector3 optimizedViewportPoint = viewportPoint;

                Vector2 canvasSize = this.m_refCanvasRectTransform.rect.size;

                bool isBehind = viewportPoint.z < 0.0f;

                // ---------------

                //
                {

                    if (isBehind)
                    {
                        optimizedViewportPoint = new Vector3(1, 1, 0) - viewportPoint;
                    }

                    Vector3 shiftedXY = optimizedViewportPoint - new Vector3(0.5f, 0.5f, optimizedViewportPoint.z);

                    float absX = Mathf.Abs(shiftedXY.x);
                    float absY = Mathf.Abs(shiftedXY.y);

                    if (absX > 0.5f || absY > 0.5f)
                    {

                        if (absY >= absX && !Mathf.Approximately(absY, 0.0f))
                        {
                            shiftedXY *= (0.5f / absY);
                        }

                        else if (absY <= absX && !Mathf.Approximately(absX, 0.0f))
                        {
                            shiftedXY *= (0.5f / absX);
                        }

                        optimizedViewportPoint = shiftedXY + new Vector3(0.5f, 0.5f, 0.0f);

                    }

                }

                // show or hide
                {

                    if (!this.ShouldShowUi(optimizedViewportPoint.x, optimizedViewportPoint.y, isBehind))
                    {
                        this.startHidingIfNeeded();
                    }

                    else
                    {

                        // anchoredPosition
                        {

                            Vector2 anchoredPosition = Vector2.zero;

                            if (this.m_stayOnScreenBorder)
                            {

                                optimizedViewportPoint.x = Mathf.Clamp(optimizedViewportPoint.x, this.m_stayOnScreenBorderOffsetX, 1.0f - this.m_stayOnScreenBorderOffsetX);
                                optimizedViewportPoint.y = Mathf.Clamp(optimizedViewportPoint.y, this.m_stayOnScreenBorderOffsetY, 1.0f - this.m_stayOnScreenBorderOffsetY);

                                anchoredPosition = new Vector2(
                                    optimizedViewportPoint.x * canvasSize.x,
                                    optimizedViewportPoint.y * canvasSize.y
                                    );

                            }

                            else
                            {
                                anchoredPosition = new Vector2(
                                    viewportPoint.x * canvasSize.x,
                                    viewportPoint.y * canvasSize.y
                                    );
                            }

                            this.refRectTransform.anchoredPosition = anchoredPosition;

                        }

                        // startShowing
                        {
                            this.startShowingIfNeeded();
                        }

                    }

                }

            }

        }

    }

}
