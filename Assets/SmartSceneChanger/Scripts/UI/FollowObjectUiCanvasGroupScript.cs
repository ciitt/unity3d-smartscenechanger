﻿using SSC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SSC
{

    /// <summary>
    /// UI to follow an object with CanvasGroup
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    public class FollowObjectUiCanvasGroupScript : FollowObjectUiScript
    {

        /// <summary>
        /// Hide at Awake
        /// </summary>
        [SerializeField]
        [Tooltip("Hide at Awake")]
        protected bool m_hideAtAwake = true;

        /// <summary>
        /// Transition seconds
        /// </summary>
        [SerializeField]
        [Tooltip("Transition seconds")]
        protected float m_transitionSeconds = 0.2f;

        /// <summary>
        /// Reference to CanvasGroup
        /// </summary>
        protected CanvasGroup m_refCanvasGroup = null;

        /// <summary>
        /// Show IEnumerator
        /// </summary>
        /// <returns></returns>
        // ------------------------------------------------------------------------------------------
        protected override IEnumerator show()
        {
            
            // init
            {
                this.m_refCanvasGroup.interactable = true;
                this.m_refCanvasGroup.blocksRaycasts = true;
            }

            // show
            {

                float val = 0.0f;
                float timeCounter = 0.0f;

                float firstAlpha = this.m_refCanvasGroup.alpha;

                while (timeCounter < this.m_transitionSeconds)
                {

                    val = timeCounter / this.m_transitionSeconds;


                    this.m_refCanvasGroup.alpha = Mathf.Lerp(firstAlpha, 1.0f, val);

                    timeCounter += Time.deltaTime;

                    yield return null;

                }

            }

            // finish
            {
                this.m_refCanvasGroup.alpha = 1.0f;
            }

        }

        /// <summary>
        /// Hide IEnumerator
        /// </summary>
        /// <returns></returns>
        // ------------------------------------------------------------------------------------------
        protected override IEnumerator hide()
        {

            // init
            {
                this.m_refCanvasGroup.interactable = false;
                this.m_refCanvasGroup.blocksRaycasts = false;
            }

            // hide
            {

                float val = 0.0f;
                float timeCounter = 0.0f;

                float firstAlpha = this.m_refCanvasGroup.alpha;

                while (timeCounter < this.m_transitionSeconds)
                {

                    val = timeCounter / this.m_transitionSeconds;

                    this.m_refCanvasGroup.alpha = Mathf.Lerp(firstAlpha, 0.0f, val);

                    timeCounter += Time.deltaTime;

                    yield return null;

                }

            }

            // finish
            {
                this.m_refCanvasGroup.alpha = 0.0f;
            }

        }

        /// <summary>
        /// Awake
        /// </summary>
        // ------------------------------------------------------------------------------------------
        protected virtual void Awake()
        {

            this.m_refCanvasGroup = this.GetComponent<CanvasGroup>();

            if (this.m_hideAtAwake)
            {
                this.m_refCanvasGroup.alpha = 0.0f;
                this.m_refCanvasGroup.interactable = false;
                this.m_refCanvasGroup.blocksRaycasts = false;
                this.m_nowShowing = false;
            }

        }

    }

}
